class Jogador{
    constructor(nome, numero){
        this._nome = nome;
        this._numero = numero;
    }
}

class Time {
    constructor(nome, tipoEsporte, status=true, liga, jogadores=[]){
        this.nome = nome;
        this.tipoEsporte = tipoEsporte;
        this.status = status;
        this.liga = liga;
        this.jogadores = [];
    }

    adicionarJogador(jogador){
        this.jogadores.push(jogador);
    }

    jogadorPorNome(nome){
        let resultado = [];
        for(let i = 0; i < this.jogadores.length; i++){
            let jogador = this.jogadores[i];
            if(jogador._nome === nome){
                resultado.push(jogador);
            }
        }
        return resultado;
    }

    jogadorPorNumero(numero){
        for(let i = 0; i < this.jogadores.length; i++){
            let jogador = this.jogadores[i];
            if(jogador._numero === numero){
                return jogador;
            }
        }   
    }


}

let time = new Time("ZeroCreativity", "Boliche", true, "A");
time.adicionarJogador(new Jogador("Legolas", 10));
console.log(time);

let jogador = time.jogadorPorNome("Legolas");
let jogador1 = time.jogadorPorNumero(10);
console.log(jogador);
console.log(jogador1);