import React, { useState, useEffect } from 'react';
import EpisodioAPI from '../../models/EpisodioAPI';

function Formulario( props ){
  const [ qntVezesAssistido, setQntVezesAssistido ] = useState( 1 );
  const [ nome, setNome ] = useState( 'Marcos' );

  useEffect( () => {
    this.EpisodioAPI = new EpisodioAPI();
    this.EpisodioAPI.buscarNotas();
  }, [ nome ] )

  return(
    <React.Fragment>
      <h1>Quantidade de vezes Assistidas: { qntVezesAssistido }</h1>
      <button type='button' onClick={ () => setQntVezesAssistido( qntVezesAssistido + 1 ) } >Já Assisti</button>
      <h2>Nome: { nome }</h2>
      <input type='text' onBlur={ evt => setNome( evt.target.value ) } />
    </React.Fragment>
  )
}

export default Formulario;