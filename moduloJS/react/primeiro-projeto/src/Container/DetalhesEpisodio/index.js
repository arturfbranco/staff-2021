import React from 'react';
import BotaoUi from '../../Components/BotaoUi';

export default class DetalhesEpisodio extends React.Component{

  constructor(props){
    super( props );
    this.state = {
      episodio: this.props.location.state,
      detalhes: this.props.location.state.detalhes[0]
    }
  }
 
  get temporadaFormatada(){
    return `Temporada: ${ this.state.episodio.temporada }`
  }

  get episodioFormatado(){
    return `Episódio: ${ this.state.episodio.ordem }`
  }

  get duracaoFormatada(){
    return `Duração: ${ this.state.episodio.duracao } min`
  }

  get sinopseFormatada(){
    return `Sinopse: ${ this.state.detalhes.sinopse }`
  }

  get dataFormatada(){
    const dataFragmentada = this.state.detalhes.dataEstreia.split( 'T' )[0].split( '-' );
    return `Data de estreia: ${ dataFragmentada[2] }/${ dataFragmentada[1] }/${ dataFragmentada[0] }`;
  }

  get notaFormatada(){
    return `Minha avaliação: ${ this.state.episodio.nota ? this.state.episodio.nota : '*' } / 5`
  }

  get notaImdbFormatada(){
    return `Nota IMDB: ${ (this.state.detalhes.notaImdb * 0.5).toFixed( 2 ) } / 5`
  }

  render(){
    const { nome, imagem } = this.state.episodio;

    return(
      !this.state.detalhes ?
      ( <h3>Aguarde...</h3> ) :
      <div>
        <BotaoUi link='/' classe='verde' nome='Página inicial' />
        <h2>{ nome }</h2>
        <img src={ imagem } />
        <p>{ this.temporadaFormatada }</p>
        <p>{ this.episodioFormatado }</p>
        <p>{ this.duracaoFormatada }</p>
        <p>{ this.sinopseFormatada }</p>
        <p>{ this.dataFormatada }</p>
        <p>{ this.notaFormatada }</p>
        <p>{ this.notaImdbFormatada }</p>
        
      </div>
    )

    
  }
}