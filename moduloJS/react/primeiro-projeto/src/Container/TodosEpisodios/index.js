import React from 'react';
import Episodio from '../../models/Episodio';
import { Link, link } from 'react-router-dom'
import BotaoUi from '../../Components/BotaoUi';

export default class TodosEpisodios extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      listaEpisodios: this.props.location.state.listaEpisodios,
      estreiaCrescente: true,
      duracaoCrescente: true      
    }
  }

  ordenarPorDuracao(){
    let flag = false;
    if( this.state.duracaoCrescente === true ){
      this.state.listaEpisodios.ordenarPorDuracaoCrescente();
    } else{
      this.state.listaEpisodios.ordenarPorDuracaoDecrescente();
      flag = true;
    }
    this.setState( state => { return{ ...state, listaEpisodios: this.state.listaEpisodios, duracaoCrescente: flag } } );
  }

  ordenarPorDataDeEstreia(){
    let flag = false;
    if( this.state.estreiaCrescente === true ) {
      this.state.listaEpisodios.ordenarPorEstreiaCrescente();
    } else{ 
      this.state.listaEpisodios.ordenarPorEstreiaDecrescente();
      flag = true;
    }
    this.setState( state => { return{ ...state, listaEpisodios: this.state.listaEpisodios, estreiaCrescente: flag } } );
  }

  render(){

    return(
      !this.state.listaEpisodios ?
      (<h3>Aguarde...</h3>) :
      <React.Fragment>
        <BotaoUi link='/' classe="verde" nome="Página inicial" />
        <div className="botoes">
          <h3>Modo de ordenação</h3>
          <BotaoUi classe="preto" nome="Data De Estréia" metodo={ this.ordenarPorDataDeEstreia.bind( this ) } />
          <BotaoUi classe='vermelho' nome='Duração' metodo={ this.ordenarPorDuracao.bind( this ) } />
          <p>{ '\n' }</p>
        </div>
        { this.state.listaEpisodios.todos.map( ep => {
          return(
            <React.Fragment>
              <Link to={ { pathname: "/detalhes-episodio", state: ep } } >{ `S${ ep.temporada }E${ ep.ordem } - Episódio: ${ ep.nome } - Nota média: ${ ep.nota ? ep.nota : 'Não avaliado.' }` }</Link>
              <p>{ '\n' }</p>
            </React.Fragment>
          )
          
        } ) }
      </React.Fragment>
    )
  }
}