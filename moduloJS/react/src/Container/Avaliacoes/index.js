import React from 'react';
import { Link } from 'react-router-dom'

import BotaoUi from '../../Components/BotaoUi';
import EpisodioAPI from '../../models/EpisodioAPI';

export default class ListaAvaliacoes extends React.Component{
  constructor( props ){
    super( props );
    this.episodiosAPI = new EpisodioAPI();
    this.state = {
      notas: ''
    }
  }

  render(){
    const { listaEpisodios } = this.props.location.state;
    return (
      <React.Fragment>
        <BotaoUi classe="preto" link="/" nome="Página Inicial" />
        {
          listaEpisodios.avaliados && (
            listaEpisodios.avaliados.map( ( ep ) => {
              return (
                <div>
                  <Link to={ { pathname: "/detalhes-episodio", state: ep } }>{ `Episódio: ${ ep.nome } - Nota média: ${ ep.nota } - Temporada: ${ ep.temporada } - Número: ${ ep.ordem }` }</Link>
                  <p>{'\n'}</p>
                </div>
              )
            })
          )
        }
      </React.Fragment>
    );
  }  
} 
