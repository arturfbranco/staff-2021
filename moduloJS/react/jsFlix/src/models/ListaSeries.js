import PropTypes from 'prop-types';
import Serie from './Serie'

const ordenarPorUltimoNome = ( pessoas ) => pessoas.sort( ( a, b ) => a.split(' ')[1] < b.split(' ')[1]
                                                                   ? -1
                                                                   : a.split(' ')[1] > b.split(' ')[1] 
                                                                   ? 1
                                                                   : a.split(' ')[0] < b.split(' ')[0]
                                                                   ? -1 
                                                                   : a.split(' ')[0] > b.split(' ')[0] 
                                                                   ? 1 
                                                                   : 0 
                                                    );

const imprimirNomes = ( pessoas ) => ordenarPorUltimoNome( pessoas ).join( '\n' );

const buscarSerieComAbreviacao = ( series ) => series.find( serie => serie.elenco.every( nome => nome.includes( '.' ) ) );

export default class ListaSeries {
  constructor(){
    this.todas = series;
  }

  invalidas = () => this.todas.filter( serie => ( ( serie.anoEstreia > 2021 ) || ( Object.values( serie ).includes( null ) ) ) );

  filtrarPorAno = ( ano ) => this.todas.filter( serie => serie.anoEstreia <= ano );

  procurarPorNome = ( nome ) => this.todas.some( serie => serie.elenco.some( ator => ator.includes( nome ) ) );

  mediaDeEpisodios = () => parseInt( this.todas.map( serie => serie.numeroEpisodios ).reduce( ( acc, atual ) => acc + atual ) / this.todas.length );

  totalSalarios = ( indice ) => `R$ ${ 100000 * this.todas[indice].diretor.length + 40000 * this.todas[indice].elenco.length },00`;

  queroGenero = ( genero ) => this.todas.filter(serie => serie.genero.includes( genero ) );

  queroTitulo = ( titulo ) => this.todas.filter( serie => serie.titulo.includes( titulo ) );

  creditos = ( indice ) => 'DIRETORES:\n' + imprimirNomes( this.todas[ indice ].diretor ) + '\n \nELENCO:\n' + imprimirNomes( this.todas[ indice ].elenco );

  hashtag = () => `#${ buscarSerieComAbreviacao( this.todas ).elenco.map( nome => nome.split(' ').find( parteNome => parteNome.includes( '.' ) ) ).join('').replaceAll('.', '') }`;

}

ListaSeries.PropTypes = {

  invalidas: PropTypes.array,
  filtrarPorAno: PropTypes.array,
  procurarPorNome: PropTypes.array,
  mediaDeEpisodios: PropTypes.array,
  totalSalarios: PropTypes.array,
  queroGenero: PropTypes.array,
  queroTitulo: PropTypes.array,
  creditos: PropTypes.array,
  hashtag: PropTypes.array

}

const series = [
  {
    "titulo": "Stranger Things",
    "anoEstreia": 2016,
    "diretor": [
      "Matt Duffer",
      "Ross Duffer"
    ],
    "genero": [
      "Suspense",
      "Ficcao Cientifica",
      "Drama"
    ],
    "elenco": [
      "Winona Ryder",
      "David Harbour",
      "Finn Wolfhard",
      "Millie Bobby Brown",
      "Gaten Matarazzo",
      "Caleb McLaughlin",
      "Natalia Dyer",
      "Charlie Heaton",
      "Cara Buono",
      "Matthew Modine",
      "Noah Schnapp"
    ],
    "temporadas": 2,
    "numeroEpisodios": 17,
    "distribuidora": "Netflix"
  },
  {
    "titulo": "Game Of Thrones",
    "anoEstreia": 2011,
    "diretor": [
      "David Benioff",
      "D. B. Weiss",
      "Carolyn Strauss",
      "Frank Doelger",
      "Bernadette Caulfield",
      "George R. R. Martin"
    ],
    "genero": [
      "Fantasia",
      "Drama"
    ],
    "elenco": [
      "Peter Dinklage",
      "Nikolaj Coster-Waldau",
      "Lena Headey",
      "Emilia Clarke",
      "Kit Harington",
      "Aidan Gillen",
      "Iain Glen ",
      "Sophie Turner",
      "Maisie Williams",
      "Alfie Allen",
      "Isaac Hempstead Wright"
    ],
    "temporadas": 7,
    "numeroEpisodios": 67,
    "distribuidora": "HBO"
  },
  {
    "titulo": "The Walking Dead",
    "anoEstreia": 2010,
    "diretor": [
      "Jolly Dale",
      "Caleb Womble",
      "Paul Gadd",
      "Heather Bellson"
    ],
    "genero": [
      "Terror",
      "Suspense",
      "Apocalipse Zumbi"
    ],
    "elenco": [
      "Andrew Lincoln",
      "Jon Bernthal",
      "Sarah Wayne Callies",
      "Laurie Holden",
      "Jeffrey DeMunn",
      "Steven Yeun",
      "Chandler Riggs ",
      "Norman Reedus",
      "Lauren Cohan",
      "Danai Gurira",
      "Michael Rooker ",
      "David Morrissey"
    ],
    "temporadas": 9,
    "numeroEpisodios": 122,
    "distribuidora": "AMC"
  },
  {
    "titulo": "Band of Brothers",
    "anoEstreia": 20001,
    "diretor": [
      "Steven Spielberg",
      "Tom Hanks",
      "Preston Smith",
      "Erik Jendresen",
      "Stephen E. Ambrose"
    ],
    "genero": [
      "Guerra"
    ],
    "elenco": [
      "Damian Lewis",
      "Donnie Wahlberg",
      "Ron Livingston",
      "Matthew Settle",
      "Neal McDonough"
    ],
    "temporadas": 1,
    "numeroEpisodios": 10,
    "distribuidora": "HBO"
  },
  {
    "titulo": "The JS Mirror",
    "anoEstreia": 2017,
    "diretor": [
      "Lisandro",
      "Jaime",
      "Edgar"
    ],
    "genero": [
      "Terror",
      "Caos",
      "JavaScript"
    ],
    "elenco": [
      "Amanda de Carli",
      "Alex Baptista",
      "Gilberto Junior",
      "Gustavo Gallarreta",
      "Henrique Klein",
      "Isaias Fernandes",
      "João Vitor da Silva Silveira",
      "Arthur Mattos",
      "Mario Pereira",
      "Matheus Scheffer",
      "Tiago Almeida",
      "Tiago Falcon Lopes"
    ],
    "temporadas": 5,
    "numeroEpisodios": 40,
    "distribuidora": "DBC"
  },
  {
    "titulo": "Mr. Robot",
    "anoEstreia": 2018,
    "diretor": [
      "Sam Esmail"
    ],
    "genero": [
      "Drama",
      "Techno Thriller",
      "Psychological Thriller"
    ],
    "elenco": [
      "Rami Malek",
      "Carly Chaikin",
      "Portia Doubleday",
      "Martin Wallström",
      "Christian Slater"
    ],
    "temporadas": 3,
    "numeroEpisodios": 32,
    "distribuidora": "USA Network"
  },
  {
    "titulo": "Narcos",
    "anoEstreia": 2015,
    "diretor": [
      "Paul Eckstein",
      "Mariano Carranco",
      "Tim King",
      "Lorenzo O Brien"
    ],
    "genero": [
      "Documentario",
      "Crime",
      "Drama"
    ],
    "elenco": [
      "Wagner Moura",
      "Boyd Holbrook",
      "Pedro Pascal",
      "Joann Christie",
      "Mauricie Compte",
      "André Mattos",
      "Roberto Urbina",
      "Diego Cataño",
      "Jorge A. Jiménez",
      "Paulina Gaitán",
      "Paulina Garcia"
    ],
    "temporadas": 3,
    "numeroEpisodios": 30,
    "distribuidora": null
  },
  {
    "titulo": "Westworld",
    "anoEstreia": 2016,
    "diretor": [
      "Athena Wickham"
    ],
    "genero": [
      "Ficcao Cientifica",
      "Drama",
      "Thriller",
      "Acao",
      "Aventura",
      "Faroeste"
    ],
    "elenco": [
      "Anthony I. Hopkins",
      "Thandie N. Newton",
      "Jeffrey S. Wright",
      "James T. Marsden",
      "Ben I. Barnes",
      "Ingrid N. Bolso Berdal",
      "Clifton T. Collins Jr.",
      "Luke O. Hemsworth"
    ],
    "temporadas": 2,
    "numeroEpisodios": 20,
    "distribuidora": "HBO"
  },
  {
    "titulo": "Breaking Bad",
    "anoEstreia": 2008,
    "diretor": [
      "Vince Gilligan",
      "Michelle MacLaren",
      "Adam Bernstein",
      "Colin Bucksey",
      "Michael Slovis",
      "Peter Gould"
    ],
    "genero": [
      "Acao",
      "Suspense",
      "Drama",
      "Crime",
      "Humor Negro"
    ],
    "elenco": [
      "Bryan Cranston",
      "Anna Gunn",
      "Aaron Paul",
      "Dean Norris",
      "Betsy Brandt",
      "RJ Mitte"
    ],
    "temporadas": 5,
    "numeroEpisodios": 62,
    "distribuidora": "AMC"
  }
].map( serie => new Serie ( serie ) );