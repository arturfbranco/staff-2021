import React, { Component } from 'react';

import ListaSeries from '../models/ListaSeries';

import './App.css';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.listaSeries = new ListaSeries();
       
    console.log( this.listaSeries.invalidas() );
    console.log( this.listaSeries.filtrarPorAno( 2017 ) );
    console.log( this.listaSeries.procurarPorNome( 'David' ) );
    console.log( this.listaSeries.mediaDeEpisodios() );
    console.log( this.listaSeries.totalSalarios( 5 ) );
    console.log( this.listaSeries.queroGenero( 'Suspense' ) );
    console.log( this.listaSeries.queroTitulo( 'the' ) );
    console.log( this.listaSeries.creditos( 1 ) );
    console.log( this.listaSeries.hashtag() );
  }
  
  render() {
    return (
      <div className="App">
      </div>
    );
  }
}