/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

const executarBusca = ( pokemonId ) => {
  const pokeApi = new PokeApi();
  pokeApi.buscarEspecifico( pokemonId ).then( pokemon => {
    const poke = new Pokemon( pokemon );
    renderizar( poke );
  } ).catch( () => {
    idInvalido( pokemonId );
  } );
}

const registrarEExecutarSorteio = ( sorteado ) => {
  localStorage.setItem( 'registro', `${ localStorage.getItem( 'registro' ) } ${ sorteado }` );
  executarBusca( sorteado );
}

const sortear = () => {
  const sortearIntervalo = Math.floor( Math.random() * 5 ) + 1;
  let sorteado;
  if ( sortearIntervalo !== 5 ) {
    sorteado = Math.floor( Math.random() * 898 ) + 1;
  } else {
    sorteado = Math.floor( Math.random() * 220 ) + 10001;
  }
  return localStorage.getItem( 'registro' ).split( ' ' ).indexOf( sorteado.toString() ) === -1 ? registrarEExecutarSorteio( sorteado ) : sortear();
}

if ( localStorage.getItem( 'registro' ) === null ) {
  localStorage.setItem( 'registro', '' );
}

const pokemonId = document.getElementById( 'pokemon-id' );
pokemonId.addEventListener( 'blur', () => {
  validarIdDiferente( document.getElementById( 'pokemon-id' ).value );
} );

const anterior = document.getElementById( 'anterior' );
anterior.addEventListener( 'click', () => {
  validarAnteriorProximo( 'anterior' );
} );

const estouComSorte = document.getElementById( 'estou-com-sorte' );
estouComSorte.addEventListener( 'click', () => {
  validarRegistroComVaga();
} );

const proximo = document.getElementById( 'proximo' );
proximo.addEventListener( 'click', () => {
  validarAnteriorProximo( 'proximo' );
} );
