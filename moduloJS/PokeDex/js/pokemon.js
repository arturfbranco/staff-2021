class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objApi ) {
    this._id = objApi.id;
    this._nome = objApi.name;
    this._altura = objApi.height;
    this._peso = objApi.weight;
    this._tipos = objApi.types;
    this._estatisticas = objApi.stats;
    this._imagem = objApi.sprites.front_default;
  }

  get id() {
    return `Id: ${ this._id }`;
  }

  get nome() {
    return this._nome;
  }

  get altura() {
    return `${ this._altura * 10 } Cm`;
  }

  get peso() {
    return `${ this._peso / 10 } Kg`;
  }

  get tipos() {
    return this._tipos;
  }

  get estatisticas() {
    return this._estatisticas;
  }

  get imagem() {
    return this._imagem;
  }
}
