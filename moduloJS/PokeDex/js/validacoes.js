/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

const buscarIdAtual = () => parseInt( document.getElementById( 'dadosPokemon' ).querySelector( '.id' ).innerText.split( ' ' )[1], 10 );

const validarLimiteIntervalos = ( idAtual ) => {
  if ( idAtual === 10001 ) {
    executarBusca( 898 );
  }
  if ( idAtual === 898 ) {
    executarBusca( 10001 );
  }
}

const validarAnterior = ( idAtual ) => ( ( idAtual > 1 && idAtual <= 898 ) || ( idAtual > 10001 && idAtual <= 10220 ) ? executarBusca( idAtual - 1 ) : validarLimiteIntervalos( idAtual ) );

const validarProximo = ( idAtual ) => ( ( idAtual < 898 ) || ( idAtual >= 10001 && idAtual < 10220 ) ? executarBusca( idAtual + 1 ) : validarLimiteIntervalos( idAtual ) );

const validarAnteriorProximo = ( direcao ) => ( direcao === 'anterior' ? validarAnterior( buscarIdAtual() ) : validarProximo( buscarIdAtual() ) );

const validarRegistroComVaga = () => ( localStorage.getItem( 'registro' ).split( ' ' ).length <= 1116 ? sortear() : registroCheio() );

const validarIdDiferente = ( pokemonId ) => ( parseInt( pokemonId, 10 ) !== buscarIdAtual() ? executarBusca( pokemonId ) : mesmoId() );
