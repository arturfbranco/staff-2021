package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class EspacoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void buscarEspaco() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/cw/espaco/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }

    @Test
    public void salvarEspaco() throws Exception {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setValor(500.0);
        espaco.setNome("Nome");
        espaco.setQntPessoas(200);

        mockMvc
                .perform(MockMvcRequestBuilders.post("/cw/espaco/salvar/").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(espaco)))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }


}
