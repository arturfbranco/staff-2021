package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
public class Espaco_X_PacoteRepositoryTest {

    @Autowired
    private Espaco_X_PacoteRepository repository;

    @Autowired
    private EspacoRepository espacoRepository;
    @Test
    public void salvarEspacoPacote(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Praia");
        espaco.setQntPessoas(1000);
        espaco.setValor(1.0);
        Espaco_X_PacoteEntity ep = new Espaco_X_PacoteEntity();
        ep.setEspaco(espaco);
        ep.setPrazo(30);
        ep.setQuantidade(30);
        ep.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        Espaco_X_PacoteEntity salvo = repository.save(ep);
        assertEquals(ep.getPrazo(), repository.findById(salvo.getId()).get().getPrazo());
    }

    @Test
    public void editarEspacoPacote(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Praia");
        espaco.setQntPessoas(1000);
        espaco.setValor(1.0);
        Espaco_X_PacoteEntity ep2 = new Espaco_X_PacoteEntity();
        ep2.setEspaco(espaco);
        ep2.setPrazo(30);
        ep2.setQuantidade(30);
        ep2.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        Espaco_X_PacoteEntity salvo = repository.save(ep2);
        ep2.setQuantidade(1000);
        ep2.setId(salvo.getId());
        Espaco_X_PacoteEntity editado = repository.save(ep2);
        assertEquals(ep2.getQuantidade(), repository.findById(salvo.getId()).get().getQuantidade());
    }
}
