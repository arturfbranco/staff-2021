package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ContatoRepositoryTest {

    @Autowired
    private ContatoRepository repository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void salvarContato(){
        TipoContatoEntity tp = new TipoContatoEntity();
        tp.setNome("telefone");
        tipoContatoRepository.save(tp);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(tp);
        contato.setContato("5551999445583");
        repository.save(contato);
        assertEquals(contato.getContato(), repository.findByContato(contato.getContato()).get().getContato());
    }

}
