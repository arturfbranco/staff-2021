package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
public class SaldoClienteRepositoryTest {

    @Autowired
    private SaldoClienteRepository repository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Test
    public void salvarSaldoCliente(){

        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("telefone");
        tp2.setNome("email");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont.setContato("51995544835");
        cont2.setContato("paula@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        Character[] cpf = {'2', '5', '3', '2', '6', '4', '8', '2', '7', '1', '2', '0'};
        cliente.setCpf(cpf);
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        ClienteEntity clienteSalvo = clienteRepository.save(cliente);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Berlin");
        espaco.setValor(12.0);
        espaco.setQntPessoas(500000);
        EspacoEntity espacoSalvo = espacoRepository.save(espaco);

        SaldoClienteEntity saldo = new SaldoClienteEntity();
        SaldoClienteId id = new SaldoClienteId();
        id.setId_cliente(clienteSalvo.getId());
        id.setId_espaco(espacoSalvo.getId());
        saldo.setId(id);
        saldo.setCliente(clienteSalvo);
        saldo.setEspaco(espacoSalvo);
        saldo.setTipoContratacao(TipoContratacaoEnum.HORA);
        saldo.setQuantidade(40);
        LocalDate dataAtual = LocalDate.now();
        saldo.setVencimento(dataAtual);
        SaldoClienteEntity salvo = repository.save(saldo);
        assertEquals(saldo.getCliente().getCpf(), repository.findById(salvo.getId()).get().getCliente().getCpf());
        assertEquals(saldo.getEspaco().getNome(), repository.findByQuantidade(saldo.getQuantidade()).get().getEspaco().getNome());


    }
}
