package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class PagamentoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void buscarPagamentos() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/cw/pagamento/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }

    @Test
    public void salvarPagamento() throws  Exception{
        PagamentoEntity pag = new PagamentoEntity();
        pag.setTipoPagamento(TipoPagamentoEnum.CREDITO);
        mockMvc
                .perform(MockMvcRequestBuilders.post("/cw/pagamento/pagar/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(pag)))
                .andExpect(MockMvcResultMatchers.status().is(403));
    }
}
