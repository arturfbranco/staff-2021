package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ContratacaoRepositoryTest {

    @Autowired
    private ContratacaoRepository repository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void salvarContratacao(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("telefone");
        tp2.setNome("email");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont.setContato("51995544835");
        cont2.setContato("paula@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        Character[] cpf = {'2', '5', '3', '2', '6', '4', '8', '2', '7', '1', '2', '0'};
        cliente.setCpf(cpf);
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        clienteRepository.save(cliente);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Berlin");
        espaco.setValor(12.0);
        espaco.setQntPessoas(500000);
        espacoRepository.save(espaco);
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(cliente);
        contratacao.setEspaco(espaco);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        contratacao.setQuantidade(30);
        contratacao.setPrazo(90);
        contratacao.setDesconto(0.15);
        repository.save(contratacao);
        assertEquals(contratacao.getPrazo(), repository.findAllByPrazo(contratacao.getPrazo()).get(0).getPrazo());
        assertEquals(contratacao.getCliente(), repository.findByCliente(contratacao.getCliente()).get().getCliente());
    }

    @Test
    public void editarContratacao(){
        TipoContatoEntity tp = new TipoContatoEntity();
        TipoContatoEntity tp2 = new TipoContatoEntity();
        tp.setNome("telefone");
        tp2.setNome("email");
        tipoContatoRepository.save(tp);
        tipoContatoRepository.save(tp2);
        ContatoEntity cont = new ContatoEntity();
        ContatoEntity cont2 = new ContatoEntity();
        cont.setTipoContato(tp);
        cont2.setTipoContato(tp2);
        cont.setContato("51995544835");
        cont2.setContato("paula@gmail.com");
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(cont);
        contatos.add(cont2);
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContatos(contatos);
        cliente.setNome("Paula");
        Character[] cpf = {'2', '5', '3', '2', '6', '4', '8', '2', '7', '1', '2', '0'};
        cliente.setCpf(cpf);
        cliente.setDataNascimento(LocalDate.parse("2002-10-04"));
        clienteRepository.save(cliente);
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Berlin");
        espaco.setValor(12.0);
        espaco.setQntPessoas(500000);
        espacoRepository.save(espaco);
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(cliente);
        contratacao.setEspaco(espaco);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        contratacao.setQuantidade(30);
        contratacao.setPrazo(90);
        contratacao.setDesconto(0.15);
        ContratacaoEntity salvo = repository.save(contratacao);
        contratacao.setQuantidade(1200);
        contratacao.setId(salvo.getId());
        repository.save(contratacao);
        assertEquals(contratacao.getQuantidade(), repository.findById(salvo.getId()).get().getQuantidade());
    }
}
