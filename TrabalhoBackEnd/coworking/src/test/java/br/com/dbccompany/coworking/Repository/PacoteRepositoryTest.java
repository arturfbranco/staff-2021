package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class PacoteRepositoryTest {

    @Autowired
    private PacoteRepository repository;

    @Autowired EspacoRepository espacoRepository;

    @Test
    public void salvarPacote(){
        EspacoEntity e = new EspacoEntity();
        e.setValor(1.2);
        e.setNome("Praia");
        e.setQntPessoas(2000);
        EspacoEntity e2 = new EspacoEntity();
        e2.setValor(1.2);
        e2.setNome("Cidade");
        e2.setQntPessoas(2000);
        espacoRepository.save(e);
        espacoRepository.save(e2);
        Espaco_X_PacoteEntity ep = new Espaco_X_PacoteEntity();
        ep.setEspaco(e);
        ep.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        ep.setQuantidade(30);
        ep.setPrazo(60);
        Espaco_X_PacoteEntity ep2 = new Espaco_X_PacoteEntity();
        ep2.setEspaco(e2);
        ep2.setQuantidade(20);
        ep2.setTipoContratacao(TipoContratacaoEnum.HORA);
        ep2.setPrazo(10);
        List<Espaco_X_PacoteEntity> espacos = new ArrayList<>();
        espacos.add(ep);
        espacos.add(ep2);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2000.0);
        pacote.setEspacoPacotes(espacos);
        PacoteEntity salvo = repository.save(pacote);
        List<PacoteEntity> resultado = repository.findAll();
        assertEquals(pacote.getValor(), repository.findByValor(pacote.getValor()).get().getValor());
    }

    @Test
    public void editarEspaco(){
        EspacoEntity e = new EspacoEntity();
        e.setValor(1.2);
        e.setNome("Serra");
        e.setQntPessoas(2000);
        EspacoEntity e2 = new EspacoEntity();
        e2.setValor(1.2);
        e2.setNome("Montanha");
        e2.setQntPessoas(2000);
        espacoRepository.save(e);
        espacoRepository.save(e2);
        Espaco_X_PacoteEntity ep = new Espaco_X_PacoteEntity();
        ep.setEspaco(e);
        ep.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        ep.setQuantidade(30);
        ep.setPrazo(60);
        Espaco_X_PacoteEntity ep2 = new Espaco_X_PacoteEntity();
        ep2.setEspaco(e2);
        ep2.setQuantidade(20);
        ep2.setTipoContratacao(TipoContratacaoEnum.HORA);
        ep2.setPrazo(10);
        List<Espaco_X_PacoteEntity> espacos = new ArrayList<>();
        espacos.add(ep);
        espacos.add(ep2);
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(2000.0);
        pacote.setEspacoPacotes(espacos);
        PacoteEntity salvo = repository.save(pacote);
        pacote.setValor(500.0);
        pacote.setId(salvo.getId());
        repository.save(pacote);
        assertEquals(pacote.getValor(), repository.findById(salvo.getId()).get().getValor());
    }

}
