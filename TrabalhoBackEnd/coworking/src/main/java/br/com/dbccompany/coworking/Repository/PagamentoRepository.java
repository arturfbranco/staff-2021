package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Cliente_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import java.util.List;
import java.util.Optional;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {

    List<PagamentoEntity> findAll();
    Optional<PagamentoEntity> findById(Integer id);
    List<PagamentoEntity> findAllById(Iterable<Integer> ids);

    List<PagamentoEntity> findByClientePacote(Cliente_X_PacoteEntity clientePacote);

    List<PagamentoEntity> findByContratacao(ContratacaoEntity contratacao);

    Optional<PagamentoEntity> findByTipoPagamento(TipoPagamentoEnum tipoPagamento);
    List<PagamentoEntity> findAllByTipoPagamento(TipoPagamentoEnum tipoPagamento);

}
