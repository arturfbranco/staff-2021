package br.com.dbccompany.coworking.Exception.Cliente;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class ClienteException extends GeneralException {

    public ClienteException (String mensagem){
        super(mensagem);
    }
}
