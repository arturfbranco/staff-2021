package br.com.dbccompany.coworking.Exception.Cliente;

public class ErroAoSalvarCliente extends ClienteException{

    public ErroAoSalvarCliente(){
        super("Não foi possível registrar o cliente solicitado!");
    }
}
