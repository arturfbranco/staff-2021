package br.com.dbccompany.coworking.Exception.SaldoCliente;

public class SaldoNaoEncontrado extends SaldoClienteException {
    public SaldoNaoEncontrado(){
        super("Saldo não encontrado!");
    }
}
