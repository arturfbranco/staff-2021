package br.com.dbccompany.coworking.Exception.Contratacao;

public class NenhumaContratacaoEncontrada extends ContratacaoException {
    public NenhumaContratacaoEncontrada(){
        super("Nenhuma contradação encontrada!");
    }
}
