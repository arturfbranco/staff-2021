package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;

import java.util.List;

public class PagamentoDTO {

    private Integer id;
    private Cliente_X_PacoteDTO clientePacote;
    private ContratacaoDTO contratacao;
    private TipoPagamentoEnum tipoPagamento;
    private List<SaldoClienteDTO> saldosGerado;

    public PagamentoDTO(PagamentoEntity pagamento){
        this.id = pagamento.getId();
        this.clientePacote = pagamento.getClientePacote() != null ? new Cliente_X_PacoteDTO(pagamento.getClientePacote()) : null;
        this.contratacao = pagamento.getContratacao() != null ? new ContratacaoDTO(pagamento.getContratacao()) : null;
        this.tipoPagamento = pagamento.getTipoPagamento();
    }
    public PagamentoDTO(){

    }

    public PagamentoEntity converter(){
        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setId(this.id);
        pagamento.setClientePacote(this.clientePacote != null ? this.clientePacote.converter() : null);
        pagamento.setContratacao(this.contratacao != null ? this.contratacao.converter() : null);
        pagamento.setTipoPagamento(this.tipoPagamento);
        return pagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente_X_PacoteDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Cliente_X_PacoteDTO clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public List<SaldoClienteDTO> getSaldosGerado() {
        return saldosGerado;
    }

    public void setSaldosGerado(List<SaldoClienteDTO> saldosGerado) {
        this.saldosGerado = saldosGerado;
    }
}
