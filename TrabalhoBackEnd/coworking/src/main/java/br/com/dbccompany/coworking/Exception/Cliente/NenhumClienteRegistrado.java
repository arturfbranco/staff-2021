package br.com.dbccompany.coworking.Exception.Cliente;

public class NenhumClienteRegistrado extends ClienteException{
    public NenhumClienteRegistrado(){
        super("A lista de clientes está vazia!");
    }
}
