package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.Acesso.*;
import br.com.dbccompany.coworking.Repository.AcessoRepository;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository repository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    public List<AcessoDTO> buscarTodos() throws ListaDeAcessosVazia {
        try{
            return converterEntityDTO(repository.findAll());
        }catch (Exception e){
            throw new ListaDeAcessosVazia();
        }
    }

    public AcessoDTO buscarPorId(Integer id) throws AcessoInexistente {
        Optional<AcessoEntity> acesso = repository.findById(id);
        if(acesso.isPresent()) {
            return new AcessoDTO(acesso.get());
        }
        throw new AcessoInexistente();
    }

    private List<AcessoDTO> converterEntityDTO(List<AcessoEntity> entities){
        List<AcessoDTO> dtos = new ArrayList<>();
        for(AcessoEntity entity : entities){
            dtos.add(new AcessoDTO(entity));
        }
        return dtos;
    }

    @Transactional(rollbackFor = Exception.class)
    public AcessoDTO entrar(AcessoEntity acesso) throws ErroAoRegistrarEntrada {
        try{
            acesso.setSaldoCliente(saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get());
            TipoContratacaoEnum tipoContratacao = acesso.getSaldoCliente().getTipoContratacao();
            Integer saldo = acesso.getSaldoCliente().getQuantidade();
            if(saldo <= 0 || acesso.getSaldoCliente().getVencimento().isBefore(LocalDate.now())){
                SaldoClienteEntity saldoZerado = acesso.getSaldoCliente();
                saldoZerado.setQuantidade(0);
                saldoClienteRepository.save(saldoZerado);
                AcessoDTO dto = new AcessoDTO();
                dto.setMensagem("Saldo insuficiente ou prazo de validade vencido!");
                return dto;
            }
            if(acesso.getData() == null){
                acesso.setData(LocalDateTime.now());
            }
            acesso.setEntrada(true);
            AcessoDTO dto = new AcessoDTO(repository.save(acesso));
            dto.setMensagem(construirString(tipoContratacao, saldo));
            return dto;
        }catch (Exception e){
            throw new ErroAoRegistrarEntrada();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public AcessoDTO sair (AcessoEntity saida) throws ErroAoRegistrarSaida {
        try{
            saida.setSaldoCliente(saldoClienteRepository.findById(saida.getSaldoCliente().getId()).get());
            if(saida.getData() == null){
                saida.setData(LocalDateTime.now());
            }
            saida.setEntrada(false);
            List<AcessoEntity> acessos = repository.findAllBySaldoClienteAndEntradaOrderByData(saida.getSaldoCliente(), true);
            AcessoEntity ultimoAcesso = acessos.get(acessos.size() - 1);
            LocalDateTime dataEntrada = ultimoAcesso.getData();
            LocalDateTime dataSaida = saida.getData();
            Duration tempoGasto = Duration.between(dataEntrada, dataSaida);
            TipoContratacaoEnum tipoContratacao = saida.getSaldoCliente().getTipoContratacao();
            Integer saldoAnterior = saida.getSaldoCliente().getQuantidade();
            Integer saldoFinal = descontarSaldo(tipoContratacao, saldoAnterior, tempoGasto);
            SaldoClienteEntity saldoProcessado = saida.getSaldoCliente();
            saldoProcessado.setQuantidade(saldoFinal);
            saldoClienteRepository.save(saldoProcessado);
            AcessoDTO dto = new AcessoDTO(repository.save(saida));
            dto.setMensagem(construirString(tipoContratacao, saldoFinal));
            return dto;
        } catch (Exception e){
            throw new ErroAoRegistrarSaida();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public AcessoDTO entradaEspecial(AcessoEntity acesso) throws EntradaEspecialNaoAutorizada {
        try{
            SaldoClienteEntity saldoEspecial = new SaldoClienteEntity();
            SaldoClienteId id = new SaldoClienteId();
            id.setId_cliente(acesso.getSaldoCliente().getId().getId_cliente());
            id.setId_espaco(acesso.getSaldoCliente().getId().getId_espaco());
            saldoEspecial.setId(id);
            saldoEspecial.setCliente(clienteRepository.findById(saldoEspecial.getId().getId_cliente()).get());
            saldoEspecial.setEspaco(espacoRepository.findById(saldoEspecial.getId().getId_espaco()).get());
            saldoEspecial.setTipoContratacao(TipoContratacaoEnum.ESPECIAL);
            saldoEspecial.setQuantidade(999);
            saldoEspecial.setVencimento(LocalDate.now().plusDays(1));
            acesso.setSaldoCliente(saldoClienteRepository.save(saldoEspecial));
            acesso.setExcessao(true);
            acesso.setEntrada(true);
            acesso.setData(LocalDateTime.now());
            AcessoDTO dto = new AcessoDTO(repository.save(acesso));
            dto.setMensagem(construirString(TipoContratacaoEnum.ESPECIAL, 999));
            return dto;
        } catch (Exception e){
            throw new EntradaEspecialNaoAutorizada();
        }
    }

    private String construirString (TipoContratacaoEnum tipoContratacao, Integer saldo){
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("Tipo Contratação: ");
        mensagem.append(tipoContratacao);
        mensagem.append("; Saldo: ");
        mensagem.append(saldo);
        return mensagem.toString();
    }

    private Integer descontarSaldo( TipoContratacaoEnum tipoContratacao, Integer saldoAnterior, Duration tempoGasto ) throws ErroAoRegistrarSaida {
        if(tipoContratacao == TipoContratacaoEnum.MINUTO){
             Integer tempoGastoConvertido = (int) tempoGasto.toMinutes();
             return saldoAnterior - tempoGastoConvertido;
        }
        else if(tipoContratacao == TipoContratacaoEnum.HORA){
            Integer tempoGastoConvertido = (int) tempoGasto.toHours();
            return saldoAnterior - tempoGastoConvertido;
        }
        else if(tipoContratacao == TipoContratacaoEnum.TURNO){
            Integer tempoGastoConvertido = (int) tempoGasto.toHours() / 5;
            return saldoAnterior - tempoGastoConvertido;
        }
        else if (tipoContratacao == TipoContratacaoEnum.DIARIA){
            Integer tempoGastoConvertido = (int) tempoGasto.toDays();
            return saldoAnterior - tempoGastoConvertido;
        }
        else if(tipoContratacao == TipoContratacaoEnum.SEMANA){
            Integer tempoGastoConvertido = (int) tempoGasto.toDays() / 5;
            return saldoAnterior - tempoGastoConvertido;
        }
        else if (tipoContratacao == TipoContratacaoEnum.MES){
            Integer tempoGastoConvertido = (int) tempoGasto.toDays() / 20;
            return saldoAnterior - tempoGastoConvertido;

        }
        else if(tipoContratacao == TipoContratacaoEnum.ESPECIAL){
            return 0;
        }
        throw new ErroAoRegistrarSaida();
    }

}
