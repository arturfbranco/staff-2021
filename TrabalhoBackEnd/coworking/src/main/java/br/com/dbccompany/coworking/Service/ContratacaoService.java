package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Exception.Contratacao.ContratacaoException;
import br.com.dbccompany.coworking.Exception.Contratacao.ErroAoRegistrarContratacao;
import br.com.dbccompany.coworking.Exception.Contratacao.NenhumaContratacaoEncontrada;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Util.ConversorDinheiro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public ContratacaoDTO salvar(ContratacaoEntity contratacao) throws ErroAoRegistrarContratacao {
        try{
            contratacao.setEspaco(espacoRepository.findById(contratacao.getEspaco().getId()).get());
            contratacao.setCliente(clienteRepository.findById(contratacao.getCliente().getId()).get());
            ContratacaoEntity cont = repository.save(contratacao);
            ContratacaoDTO contratacaoDTO = new ContratacaoDTO(cont);
            String valorFinal = calcularValor(cont);
            contratacaoDTO.setValor(valorFinal);
            return contratacaoDTO;
        }catch (Exception e){
            throw new ErroAoRegistrarContratacao();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public ContratacaoDTO editar(ContratacaoEntity contratacao, Integer id) throws ErroAoRegistrarContratacao{
        try{
            contratacao.setId(id);
            return salvar(contratacao);
        }catch (Exception e){
            throw new ErroAoRegistrarContratacao();
        }

    }

    public List<ContratacaoDTO> buscarTodos() throws ContratacaoException {
        try{
            return converterEntityDTO(repository.findAll());
        }catch (Exception e){
            throw new ContratacaoException("Nenhuma contratação encontrada!");
        }
    }

    public ContratacaoDTO buscarPorId(Integer id) throws ContratacaoException {
        Optional<ContratacaoEntity> contEntity = repository.findById(id);
        if(contEntity.isPresent()){
            ContratacaoDTO contDTO = new ContratacaoDTO(contEntity.get());
            contDTO.setValor(calcularValor(contEntity.get()));
            return contDTO;
        }
        throw new ContratacaoException("Nenhuma contratação encontrada!");
    }

    private List<ContratacaoDTO> converterEntityDTO(List<ContratacaoEntity> conts) throws ErroAoRegistrarContratacao {
        List<ContratacaoDTO> dtos = new ArrayList<>();
        for(ContratacaoEntity elem : conts){
            ContratacaoDTO dto = new ContratacaoDTO(elem);
            dto.setValor(calcularValor(elem));
            dtos.add(dto);
        }
        return dtos;
    }

    private String calcularValor(ContratacaoEntity contratacao) throws ErroAoRegistrarContratacao {
        Integer quantidadeMinutos = conversorQuantidade(contratacao);
        Double valorContratacao = contratacao.getEspaco().getValor() * quantidadeMinutos;
        Double valorComDesconto = descontar(valorContratacao, contratacao.getDesconto());
        return ConversorDinheiro.doubleParaString(valorComDesconto);
    }

    private Integer conversorQuantidade(ContratacaoEntity contratacao) throws ErroAoRegistrarContratacao {
        TipoContratacaoEnum tipoContratacao = contratacao.getTipoContratacao();
        Integer quantidade = contratacao.getQuantidade();
        if(tipoContratacao == TipoContratacaoEnum.MINUTO){
            return quantidade;
        }
        else if(tipoContratacao == TipoContratacaoEnum.HORA){
            return quantidade * 60;
        }
        else if(tipoContratacao == TipoContratacaoEnum.TURNO){
            return quantidade * 60 * 5;
        }
        else if (tipoContratacao == TipoContratacaoEnum.DIARIA){
            return quantidade * 60 * 5 * 2;
        }
        else if(tipoContratacao == TipoContratacaoEnum.SEMANA){
            return quantidade * 60 * 5 * 2 * 5;
        }
        else if(tipoContratacao == TipoContratacaoEnum.MES){
            return quantidade * 60 * 5 * 2 * 5 * 4;
        }
        throw new ErroAoRegistrarContratacao();
    }

    private Double descontar(Double valorTotal, Double desconto){
        if(desconto == null){
            return valorTotal;
        }
        return valorTotal - (valorTotal * desconto);
    }
}
