package br.com.dbccompany.coworking.Exception.SaldoCliente;

public class ErroAoSalvarSaldo extends SaldoClienteException{
    public ErroAoSalvarSaldo(){
        super("Não foi possível salvar o saldo informado!");
    }
}
