package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Exception.Contratacao.ContratacaoException;
import br.com.dbccompany.coworking.Exception.Contratacao.ErroAoRegistrarContratacao;
import br.com.dbccompany.coworking.Exception.Contratacao.NenhumaContratacaoEncontrada;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import br.com.dbccompany.coworking.Util.LogsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(value = "cw/contratacao")
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> salvar (@RequestBody ContratacaoDTO contratacao){
        try{
            return new ResponseEntity<>(service.salvar(contratacao.converter()), HttpStatus.CREATED);
        }catch (ErroAoRegistrarContratacao e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> editar (@RequestBody ContratacaoDTO contratacao, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(contratacao.converter(), id), HttpStatus.CREATED);
        }catch (ErroAoRegistrarContratacao e){
            LogsGenerator.erro422(e);
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTO>> buscarTodos(){
        try{
            return new ResponseEntity<>(service.buscarTodos(), HttpStatus.OK);
        }catch (ContratacaoException e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<ContratacaoDTO> buscarId(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
        }catch (ContratacaoException e){
            LogsGenerator.erro404(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
