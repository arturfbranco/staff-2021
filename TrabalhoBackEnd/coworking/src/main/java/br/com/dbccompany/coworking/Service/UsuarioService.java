package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    public UsuarioDTO salvar(UsuarioEntity usuario) {
        BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

        usuario.setSenha(bcryptEncoder.encode(usuario.getSenha()));
        return new UsuarioDTO(repository.save(usuario));
    }
}
