package br.com.dbccompany.coworking.Exception.ClientePacote;

public class ErroAoRelacionarClienteEPacote extends ClientePacoteException{
    public ErroAoRelacionarClienteEPacote (){
        super("Não foi possível estabelecer o relacionamento entre cliente e pacote solicitados!");
    }
}
