package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {

    List<SaldoClienteEntity> findAll();
    Optional<SaldoClienteEntity> findById(SaldoClienteId id);
    List<SaldoClienteEntity> findAllById(Iterable<SaldoClienteId> ids);

    Optional<SaldoClienteEntity> findByCliente(ClienteEntity cliente);
    List<SaldoClienteEntity> findAllByCliente(ClienteEntity cliente);

    Optional<SaldoClienteEntity> findByEspaco(EspacoEntity espaco);
    List<SaldoClienteEntity> findAllByEspaco(EspacoEntity espaco);

    Optional<SaldoClienteEntity> findByTipoContratacao(TipoContratacaoEnum tipoContratacao);
    List<SaldoClienteEntity> findAllByTipoContratacao(TipoContratacaoEnum tipoContratacao);

    Optional<SaldoClienteEntity> findByQuantidade(Integer quantidade);
    List<SaldoClienteEntity> findAllByQuantidade(Integer quantidade);

    Optional<SaldoClienteEntity> findByVencimento(LocalDate vencimento);
    List<SaldoClienteEntity> findAllByVencimento(LocalDate vencimento);

    Optional<SaldoClienteEntity> findByAcessosIn(Iterable<AcessoEntity> acessos);




}
