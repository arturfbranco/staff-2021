package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class PagamentoEntity {

    @Id
    @SequenceGenerator(name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ")
    private Integer id;

    @ManyToOne
    private Cliente_X_PacoteEntity clientePacote;

    @ManyToOne
    private ContratacaoEntity contratacao;

    private TipoPagamentoEnum tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente_X_PacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Cliente_X_PacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
