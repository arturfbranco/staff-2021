package br.com.dbccompany.coworking.Exception.Acesso;

public class ErroAoRegistrarEntrada extends AcessoException{
    public ErroAoRegistrarEntrada(){
        super("Não foi possível registrar a entrada do cliente!");
    }
}
