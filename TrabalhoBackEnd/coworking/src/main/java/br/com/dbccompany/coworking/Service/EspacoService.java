package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Exception.Espaco.ErroAoRegistrarEspaco;
import br.com.dbccompany.coworking.Exception.Espaco.NenhumEspacoEncontrado;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Util.ConversorDinheiro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public EspacoDTO salvar(EspacoEntity espaco) throws ErroAoRegistrarEspaco {
        try{
            return new EspacoDTO(repository.save(espaco));
        }catch (Exception e){
            throw new ErroAoRegistrarEspaco();
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public EspacoDTO editar (EspacoEntity espaco, Integer id) throws ErroAoRegistrarEspaco {
        try{
            espaco.setId(id);
            return salvar(espaco);
        }catch (Exception e){
            throw new ErroAoRegistrarEspaco();
        }
    }

    private List<EspacoDTO> converterEntityParaDTO(List<EspacoEntity> entities) {
        List<EspacoDTO> dtos = new ArrayList<>();
        for(EspacoEntity elem : entities){
            dtos.add(new EspacoDTO(elem));
        }
        return dtos;
    }

    public List<EspacoDTO> buscarTodos() throws NenhumEspacoEncontrado {
        try{
            return converterEntityParaDTO(repository.findAll());
        }catch (Exception e){
            throw new NenhumEspacoEncontrado();
        }
    }

    public EspacoDTO buscarPorId(Integer id) throws NenhumEspacoEncontrado {
        Optional<EspacoEntity> espaco = repository.findById(id);
        if(espaco.isPresent()){
            return new EspacoDTO(espaco.get());
        }
        throw new NenhumEspacoEncontrado();
    }

    public EspacoDTO buscarPorNome(String nome)throws NenhumEspacoEncontrado{
        Optional<EspacoEntity> espaco = repository.findByNome(nome);
        if(espaco.isPresent()){
            return new EspacoDTO(espaco.get());
        }
        throw new NenhumEspacoEncontrado();
    }

}
