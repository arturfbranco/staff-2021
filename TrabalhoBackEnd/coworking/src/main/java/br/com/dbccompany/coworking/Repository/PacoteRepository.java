package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Cliente_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.Espaco_X_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.graalvm.compiler.nodes.calc.IntegerDivRemNode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface PacoteRepository extends CrudRepository<PacoteEntity, Integer> {

    List<PacoteEntity> findAll();
    Optional<PacoteEntity> findById(Integer id);
    List<PacoteEntity> findAllById(Iterable<Integer> ids);

    Optional<PacoteEntity> findByValor(Double valor);
    List<PacoteEntity> findAllByValor(Double valor);
}
