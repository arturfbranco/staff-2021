package br.com.dbccompany.coworking.Exception.Cliente;

public class ClienteInformadoNaoEncontrado extends ClienteException{
    public ClienteInformadoNaoEncontrado(){
        super("Não foi possível encontrar o cliente com os dados informados!");
    }
}
