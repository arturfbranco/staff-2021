package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ClienteDTO  {

    private Integer id;
    private String nome;
    private String cpf;
    private LocalDate dataNascimento;
    private List<ContatoDTO> contatos;
    private List<ContratacaoDTO> contratacao;

    public ClienteDTO(ClienteEntity cliente){
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.cpf = cliente.getCpf() != null ? charParaString(cliente.getCpf()) : null;
        this.dataNascimento = cliente.getDataNascimento();
        this.contatos = cliente.getContatos() != null ? conversaoContatoEntityParaDTO(cliente.getContatos()) : null;
        this.contratacao = cliente.getContratacao() != null ? conversaoContratacaoEntityParaDTO(cliente.getContratacao()) : null;
    }

    public ClienteDTO(){

    }

    public ClienteEntity converter(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setId(this.id);
        cliente.setNome(this.nome);
        cliente.setCpf(this.getCpf() != null ? stringParaChar(this.cpf) : null);
        cliente.setDataNascimento(this.dataNascimento);
        cliente.setContatos(this.getContatos() != null ? conversaoContatoDTOParaEntity(this.contatos) : null);
        cliente.setContratacao(this.getContratacao() != null ? conversaoContratacaoDTOParaEntity(this.contratacao) : null);
        return cliente;
    }

    private Character[] stringParaChar(String cpf){
        Character [] cpfChar = new Character[11];
        for(int i = 0; i < cpf.length(); i++){
            cpfChar[i] = cpf.charAt(i);
        }
        return cpfChar;
    }

    private String charParaString(Character[] cpf){
        StringBuilder cpfString = new StringBuilder();
        for(Character elem : cpf){
            cpfString.append(elem);
        }
        return cpfString.toString();

    }

    private List<ContatoDTO> conversaoContatoEntityParaDTO(List<ContatoEntity> contatos){
        List<ContatoDTO> contatoDTO = new ArrayList<>();
        for(ContatoEntity contato : contatos){
            contatoDTO.add(new ContatoDTO(contato));
        }
        return contatoDTO;
    }

    private List<ContatoEntity> conversaoContatoDTOParaEntity(List<ContatoDTO> contatos){
        List<ContatoEntity> contatoEntity = new ArrayList<>();
        for(ContatoDTO contato : contatos){
            contatoEntity.add(contato.converter());
        }
        return contatoEntity;
    }

    private List<ContratacaoDTO> conversaoContratacaoEntityParaDTO(List<ContratacaoEntity> contratacoes){
        List<ContratacaoDTO> contratacaoDTO = new ArrayList<>();
        for(ContratacaoEntity contratacao : contratacoes){
            contratacaoDTO.add(new ContratacaoDTO(contratacao));
        }
        return contratacaoDTO;
    }

    private List<ContratacaoEntity> conversaoContratacaoDTOParaEntity(List<ContratacaoDTO> contratacoes){
        List<ContratacaoEntity> contratacaoEntity = new ArrayList<>();
        for(ContratacaoDTO contratacao : contratacoes){
            contratacaoEntity.add(contratacao.converter());
        }
        return contratacaoEntity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoDTO> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoDTO> contatos) {
        this.contatos = contatos;
    }

    public List<ContratacaoDTO> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoDTO> contratacao) {
        this.contratacao = contratacao;
    }


}
