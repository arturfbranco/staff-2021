package br.com.dbccompany.coworking.Exception.EspacoPacote;

public class ErroAoIncluirEspacoEmPacote extends EspacoPacoteException {
    public ErroAoIncluirEspacoEmPacote(){
        super("Não foi possível incluir o espaço no pacote solicitado!");
    }
}
