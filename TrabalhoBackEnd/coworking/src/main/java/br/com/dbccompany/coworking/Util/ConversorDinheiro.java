package br.com.dbccompany.coworking.Util;

import java.text.NumberFormat;
import java.util.Locale;

public class ConversorDinheiro {

    public static String doubleParaString(Double valor){
        StringBuilder novoValor = new StringBuilder();
        novoValor.append("R$ ");
        novoValor.append(valor);
        return novoValor.toString().replace(".", ",") + "0";
    }

    public static Double stringParaDouble(String valor) {
        String novoValor = valor.replace("R$", "")
                .replace(".", "")
                .replace(" ", "")
                .replace(",", ".");
        return Double.parseDouble(novoValor);
    }
}
