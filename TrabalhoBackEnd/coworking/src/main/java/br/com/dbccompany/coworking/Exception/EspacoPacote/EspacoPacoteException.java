package br.com.dbccompany.coworking.Exception.EspacoPacote;

import br.com.dbccompany.coworking.Exception.GeneralException;

public class EspacoPacoteException extends GeneralException {

    public EspacoPacoteException (String mensagem){
        super(mensagem);
    }
}
