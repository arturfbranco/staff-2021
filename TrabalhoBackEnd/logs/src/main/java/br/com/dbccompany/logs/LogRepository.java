package br.com.dbccompany.logs;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface LogRepository extends MongoRepository<LogEntity, String> {

    List<LogEntity> findALlByCodigo(String codigo);

    List<LogEntity>  findALlByTipo(String tipo);
}
