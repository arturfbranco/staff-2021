package br.com.dbccompany.logs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LogService {

    @Autowired
    private LogRepository repository;

    public LogDTO salvar (LogEntity log){
        return new LogDTO(repository.save(log));
    }

    public List<LogDTO> buscarTodos(){
        return logEntityDTO(repository.findAll());
    }

    public LogDTO buscarId(String id){
        Optional<LogEntity> log = repository.findById(id);
        if(log.isPresent()){
            return new LogDTO(log.get());
        }
        return null;
    }

    public List<LogDTO> buscarTodosPorCodigo(String codigo){
        return logEntityDTO(repository.findALlByCodigo(codigo));
    }

    public List<LogDTO> buscarTodosPorTipo(String tipo){
        return logEntityDTO(repository.findALlByTipo(tipo));
    }

    private List<LogDTO> logEntityDTO(List<LogEntity> entities){
        List<LogDTO> dtos = new ArrayList<>();
        for(LogEntity elem : entities){
            dtos.add(new LogDTO(elem));
        }
        return dtos;
    }
}
