package br.com.dbccompany.logs;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class LogControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void buscarTodosLogs() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/logs/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void salvarLog() throws Exception{
        LogEntity log = new LogEntity();
        log.setCodigo("222");
        log.setDescricao("Teste de integração.");
        log.setTipo("INFO");
        mockMvc
                .perform(MockMvcRequestBuilders.post("/logs/salvar").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(log)))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

}
