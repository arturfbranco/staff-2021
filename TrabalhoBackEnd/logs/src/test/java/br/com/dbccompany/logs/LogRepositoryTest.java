package br.com.dbccompany.logs;

import jdk.internal.net.http.common.Log;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@DataMongoTest
public class LogRepositoryTest {

    @Autowired
    private LogRepository repository;

    @Test
    public void buscarPorCodigo(){
        LogEntity log = new LogEntity();
        log.setTipo("WARN");
        log.setCodigo("304");
        log.setDescricao("Mensagem de teste do Log.");
        repository.save(log);
        List<LogEntity> logs = repository.findALlByCodigo(log.getCodigo());

        assertEquals(log.getCodigo(), logs.get(logs.size() - 1).getCodigo());
    }

    @Test
    public void buscarPorTipo(){
        LogEntity log = new LogEntity();
        log.setTipo("INFO");
        log.setDescricao("Teste de log.");
        repository.save(log);
        assertEquals(log.getTipo(), repository.findALlByTipo(log.getTipo())
                .get(repository.findALlByTipo(log.getTipo()).size() - 1).getTipo());
    }
}
