public class Personagem {
    protected String nome;
    protected int experiencia, qntExperienciaPorAtaque;
    protected Inventario inventario;
    protected double vida;
    protected Status status;
    protected double qntDano;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.experiencia = 0;
        this.inventario = new Inventario();
        this.qntExperienciaPorAtaque = 1;
        this.qntDano = 0.0;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void setNome( String nome ){
        this.nome = nome;
    }
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    protected void aumentarXP(){
        this.experiencia += qntExperienciaPorAtaque;
    }
    
    public double getVida (){
        return this.vida;
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
    
    public void ganharItem(Item item){
        this.inventario.adicionar(item);
    }
    
    public void perderItem(Item item){
        this.inventario.remover(item);
    }
    
    private boolean podeSofrerDano(){
        return this.vida > 0;
    }
    
    private Status validacaoStatus(){
        return this.vida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }
    
    public void sofrerDano () {
        if (this.podeSofrerDano()) {
            this.vida -= this.vida >= this.qntDano ? this.qntDano : 0;
            this.status = this.validacaoStatus();
        }
    }
}
