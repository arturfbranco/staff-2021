

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class AnaoTest {
    
    @Test
    public void anaoDeveNascerCom110DeVida(){
        Anao anao = new Anao("Gimli");
        assertEquals(110.0, anao.getVida(), 0.1);
        assertEquals(Status.RECEM_CRIADO, anao.getStatus());
    }
        
    @Test
    public void anaoPerde10DeVida(){
        Anao anao = new Anao ("Alvo");
        anao.sofrerDano();
        assertEquals(100.0, anao.getVida(), 0.1);
    }
    
    @Test
    public void anaoMorreENaoFicaComVidaNegativa (){
        Anao anao = new Anao("Alvo");
        for ( int i = 0; i < 30; i++ ){
            anao.sofrerDano();
        }
        assertEquals(Status.MORTO, anao.getStatus());
        assertEquals(0, anao.getVida(), 0.1);
    }
    
    @Test
    public void anaoNasceComUmEscudo(){
        Anao anao = new Anao ("Gimli");
        Item esperado = new Item(1, "Escudo");
        Item resultado = anao.getInventario().obter(0);
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void anaoPerde10DeVidaDepoisEquipaEscudoEPerdeApenasMais5DeVida(){
        Anao anao = new Anao("Gimli");
        anao.sofrerDano();
        assertEquals(100.0, anao.getVida(), 0.1);
        anao.equiparEDesequiparEscudo();
        anao.sofrerDano();
        assertEquals(95.0, anao.getVida(), 0.1);
    }
    
    @Test
    public void anaoGanhaItemNovo(){
        Anao anao = new Anao ("Gimli");
        anao.ganharItem(new Item(5, "Charuto"));
        assertEquals("Charuto", anao.getInventario().obter(1).getDescricao());
    }
    
    @Test
    public void anaoPerdeItem(){
        Anao anao = new Anao("Gimli");
        Item caneta = new Item (1, "Caneta");
        anao.perderItem(caneta);
        assertEquals(1, anao.getInventario().getItens().size());
    }
    
}

