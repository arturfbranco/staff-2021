import java.util.*;

public class ElfoDaLuz extends Elfo {
    private final double QNT_VIDA_GANHA = 10;
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    private int ataquesEspada;
    
    {
    this.ataquesEspada = 0;
    }
    
    public ElfoDaLuz( String nome ){
        super(nome);
        super.qntDano = 21;
        super.ganharItem(new ItemSempreExistente (1, DESCRICOES_VALIDAS.get(0)));
    }
    
    public void perderItem(Item item){
        if (!item.getDescricao().equals(DESCRICOES_VALIDAS.get(0)) ){
            this.inventario.remover( item );
        }
    }
    
    private boolean devePerderVida(){
        return this.ataquesEspada % 2 == 1;
    }
    
    private Item getEspada(){
        return this.getInventario().buscar( DESCRICOES_VALIDAS.get(0) );
    }
    
    private void ganharVida(){
        super.vida += QNT_VIDA_GANHA;
    }
    
    public void ataqueEspada(Anao anao){
        Item espada = getEspada();
        if ( espada.getQuantidade() > 0 ) {
            this.ataquesEspada++;
            anao.sofrerDano();
            if (this.devePerderVida()){
                this.sofrerDano();
            } else{
                this.ganharVida();
            }
        }   
    }
    
    
}