import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoElfosTest{

    @Test
    public void exercitoAlistaElfoVerdeEElfoNoturnoMasRejeitaElfoEElfoDaLuz(){
        ExercitoElfos army = new ExercitoElfos();
        Elfo elfo = new Elfo("Brady");
        ElfoNoturno elfoN = new ElfoNoturno("Anthony");
        ElfoDaLuz elfoDL = new ElfoDaLuz("Stanford");
        ElfoVerde elfoV = new ElfoVerde("Samantha");
        army.alistar(elfo);
        army.alistar(elfoN);
        army.alistar(elfoDL);
        army.alistar(elfoV);
        assertEquals(2, army.getExercito().size());
        assertEquals(elfoN, army.getExercito().get(0));
        assertEquals(elfoV, army.getExercito().get(1));
    }
    
    @Test
    public void listaStatusElfosRecemCriadosElfosSofreuDanoElfosMortos(){
        ElfoNoturno elfo1 = new ElfoNoturno("Anthony"); 
        ElfoVerde elfo2 = new ElfoVerde("Brody"); 
        ElfoVerde elfo3 = new ElfoVerde("Courtney"); 
        ElfoNoturno elfo4 = new ElfoNoturno("Denver"); 
        ElfoVerde elfo5 = new ElfoVerde("Edward"); 
        ElfoVerde elfo6 = new ElfoVerde("Francis"); 
        ElfoNoturno elfo7 = new ElfoNoturno("Gabriel"); 
        ElfoVerde elfo8 = new ElfoVerde("Harry"); 
        ElfoVerde elfo9 = new ElfoVerde("Iago"); 
        elfo4.setQntDano(50.0);
        elfo4.sofrerDano();
        elfo5.setQntDano(50.0);
        elfo5.sofrerDano();
        elfo6.setQntDano(50.0);
        elfo6.sofrerDano();
        elfo7.setQntDano(100.0);
        elfo7.sofrerDano();
        elfo8.setQntDano(100.0);
        elfo8.sofrerDano();
        elfo9.setQntDano(100.0);
        elfo9.sofrerDano();
        ExercitoElfos army = new ExercitoElfos();
        army.alistar(elfo1);
        army.alistar(elfo2);
        army.alistar(elfo3);
        army.alistar(elfo4);
        army.alistar(elfo5);
        army.alistar(elfo6);
        army.alistar(elfo7);
        army.alistar(elfo8);
        army.alistar(elfo9);
        ArrayList<Elfo> recemCriados = army.listarStatus(Status.RECEM_CRIADO);
        ArrayList<Elfo> comDano = army.listarStatus(Status.SOFREU_DANO);
        ArrayList<Elfo> mortos = army.listarStatus(Status.MORTO);
        assertEquals(3, recemCriados.size());
        assertEquals(3, comDano.size());
        assertEquals(3, mortos.size());
        assertEquals(elfo2, recemCriados.get(1));
        assertEquals(elfo5, comDano.get(1));
        assertEquals(elfo8, mortos.get(1));
    }
    
    @Test
    public void dividirElfosVivosPorClasse(){
        ElfoNoturno elfo1 = new ElfoNoturno("Anthony"); 
        ElfoVerde elfo2 = new ElfoVerde("Brody"); 
        ElfoVerde elfo3 = new ElfoVerde("Courtney"); 
        ElfoNoturno elfo4 = new ElfoNoturno("Denver"); 
        ElfoVerde elfo5 = new ElfoVerde("Edward"); 
        ElfoVerde elfo6 = new ElfoVerde("Francis"); 
        ElfoNoturno elfo7 = new ElfoNoturno("Gabriel"); 
        ElfoVerde elfo8 = new ElfoVerde("Harry"); 
        ElfoVerde elfo9 = new ElfoVerde("Iago"); 
        elfo4.setQntDano(50.0);
        elfo4.sofrerDano();
        elfo5.setQntDano(50.0);
        elfo5.sofrerDano();
        elfo6.setQntDano(50.0);
        elfo6.sofrerDano();
        elfo7.setQntDano(100.0);
        elfo7.sofrerDano();
        elfo8.setQntDano(100.0);
        elfo8.sofrerDano();
        elfo9.setQntDano(100.0);
        elfo9.sofrerDano();
        ExercitoElfos army = new ExercitoElfos();
        army.alistar(elfo1);
        army.alistar(elfo2);
        army.alistar(elfo3);
        army.alistar(elfo4);
        army.alistar(elfo5);
        army.alistar(elfo6);
        army.alistar(elfo7);
        army.alistar(elfo8);
        army.alistar(elfo9);
        army.dividirVivosPorClasse();
        assertEquals(2, army.getVivosPorClasse().get(ElfoNoturno.class).size());
        assertEquals(4, army.getVivosPorClasse().get(ElfoVerde.class).size());
    }
    
    @Test
    public void estrategiaNoturnosPorUltimo(){
        ElfoNoturno elfo1 = new ElfoNoturno("Anthony"); 
        ElfoVerde elfo2 = new ElfoVerde("Brody"); 
        ElfoVerde elfo3 = new ElfoVerde("Courtney"); 
        ElfoNoturno elfo4 = new ElfoNoturno("Denver"); 
        ElfoVerde elfo5 = new ElfoVerde("Edward"); 
        ElfoVerde elfo6 = new ElfoVerde("Francis"); 
        ElfoNoturno elfo7 = new ElfoNoturno("Gabriel"); 
        ElfoVerde elfo8 = new ElfoVerde("Harry"); 
        ElfoVerde elfo9 = new ElfoVerde("Iago"); 
        elfo4.setQntDano(50.0);
        elfo4.sofrerDano();
        elfo5.setQntDano(50.0);
        elfo5.sofrerDano();
        elfo6.setQntDano(50.0);
        elfo6.sofrerDano();
        elfo7.setQntDano(100.0);
        elfo7.sofrerDano();
        elfo8.setQntDano(100.0);
        elfo8.sofrerDano();
        elfo9.setQntDano(100.0);
        elfo9.sofrerDano();
        ExercitoElfos army = new ExercitoElfos();
        army.alistar(elfo1);
        army.alistar(elfo2);
        army.alistar(elfo3);
        army.alistar(elfo4);
        army.alistar(elfo5);
        army.alistar(elfo6);
        army.alistar(elfo7);
        army.alistar(elfo8);
        army.alistar(elfo9);
        army.noturnosPorUltimo();
        assertEquals (ElfoVerde.class, army.getAtacantes().get(2).getClass());
        assertEquals(ElfoNoturno.class, army.getAtacantes().get(4).getClass());
    }
    
    @Test
    public void estrategiaAtaqueAlternado(){
        ElfoNoturno elfo1 = new ElfoNoturno("Anthony"); 
        ElfoVerde elfo2 = new ElfoVerde("Brody"); 
        ElfoVerde elfo3 = new ElfoVerde("Courtney"); 
        ElfoNoturno elfo4 = new ElfoNoturno("Denver"); 
        ElfoVerde elfo5 = new ElfoVerde("Edward"); 
        ElfoVerde elfo6 = new ElfoVerde("Francis"); 
        ElfoNoturno elfo7 = new ElfoNoturno("Gabriel"); 
        ElfoVerde elfo8 = new ElfoVerde("Harry"); 
        ElfoVerde elfo9 = new ElfoVerde("Iago"); 
        elfo4.setQntDano(50.0);
        elfo4.sofrerDano();
        elfo5.setQntDano(50.0);
        elfo5.sofrerDano();
        elfo6.setQntDano(50.0);
        elfo6.sofrerDano();
        elfo7.setQntDano(100.0);
        elfo7.sofrerDano();
        elfo8.setQntDano(100.0);
        elfo8.sofrerDano();
        elfo9.setQntDano(100.0);
        elfo9.sofrerDano();
        ExercitoElfos army = new ExercitoElfos();
        army.alistar(elfo2);
        army.alistar(elfo1);
        army.alistar(elfo3);
        army.alistar(elfo4);
        army.alistar(elfo5);
        army.alistar(elfo6);
        army.alistar(elfo7);
        army.alistar(elfo8);
        army.alistar(elfo9);
        army.ataqueIntercalado();
        assertEquals(4, army.getAtacantes().size());
        assertEquals(ElfoVerde.class, army.getAtacantes().get(0).getClass());
    }
    
    @Test
    public void ataqueSupremoDeElfos(){
        ElfoNoturno elfo1 = new ElfoNoturno("Anthony");
        ElfoNoturno elfo2 = new ElfoNoturno("Brody"); 
        ElfoVerde elfo3 = new ElfoVerde("Courtney"); 
        ElfoNoturno elfo4 = new ElfoNoturno("Denver"); 
        ElfoVerde elfo5 = new ElfoVerde("Edward"); 
        ElfoVerde elfo6 = new ElfoVerde("Francis"); 
        ElfoNoturno elfo7 = new ElfoNoturno("Gabriel"); 
        ElfoVerde elfo8 = new ElfoVerde("Harry");
        ElfoVerde elfo9 = new ElfoVerde("Iago");
        elfo1.getFlecha().setQuantidade(10);
        elfo2.getFlecha().setQuantidade(20);
        elfo3.getFlecha().setQuantidade(30);
        elfo6.getFlecha().setQuantidade(1);
        ExercitoElfos army = new ExercitoElfos();
        army.alistar(elfo1);
        army.alistar(elfo2);
        army.alistar(elfo3);
        army.alistar(elfo4);
        army.alistar(elfo5);
        army.alistar(elfo6);
        army.alistar(elfo7);
        army.alistar(elfo8);
        army.alistar(elfo9);
        army.ataqueSupremo();
        assertEquals(elfo3, army.getAtacantes().get(0));
        assertEquals(elfo2, army.getAtacantes().get(1));
        assertEquals(7, army.getAtacantes().size());
        assertEquals(elfo6, army.getAtacantes().get(6));
    }
    
    
    
}