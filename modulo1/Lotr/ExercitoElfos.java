import java.util.*;

public class ExercitoElfos{
    private ArrayList<Elfo> exercito;
    private HashMap<Status, ArrayList<Elfo>> porStatus;
    private HashMap<Class, ArrayList<Elfo>> vivosPorClasse;
    private ArrayList<Elfo> atacantes;
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    
    public ExercitoElfos(){
        this.exercito = new ArrayList<>();
        this.porStatus = new HashMap<>();
        this.vivosPorClasse = new HashMap<>();
        this.atacantes = new ArrayList<>();
    }
    
    public ArrayList<Elfo> getExercito(){
        return this.exercito;
    }
    
    public HashMap<Class, ArrayList<Elfo>> getVivosPorClasse(){
        return this.vivosPorClasse;
    }
    
    public ArrayList<Elfo> getAtacantes(){
        return this.atacantes;
    }
    
    public void alistar(Elfo elfo){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains( elfo.getClass() );
        if (podeAlistar){
            this.exercito.add(elfo);
            ArrayList<Elfo> elfoDeUmStatus = porStatus.get( elfo.getStatus() );
            if ( elfoDeUmStatus == null ){
                elfoDeUmStatus = new ArrayList<>();
                porStatus.put( elfo.getStatus(), elfoDeUmStatus );
            }
            elfoDeUmStatus.add( elfo );
        }
    }
    
    public ArrayList<Elfo> listarStatus(Status status){
        return this.porStatus.get( status );
    }
    
    public void atacar(Anao anao){
        for (Elfo elfo : this.atacantes){
            elfo.atirarFlecha(anao);
        }
    }
    
    private void inicializarVivosPorClasse(){
        ArrayList<Elfo> elfosVerdes = new ArrayList<>();
        ArrayList<Elfo> elfosNoturnos = new ArrayList<>();
        this.vivosPorClasse.put(TIPOS_PERMITIDOS.get(0), elfosVerdes);
        this.vivosPorClasse.put(TIPOS_PERMITIDOS.get(1), elfosNoturnos);
    }
    
    public void dividirVivosPorClasse(){
        this.inicializarVivosPorClasse();
        for (Status status : this.porStatus.keySet()){
            if (status != Status.MORTO){
                for (Elfo elfo : this.porStatus.get(status)){
                    if ( elfo.getQntFlecha() > 0 ){
                        this.vivosPorClasse.get(elfo.getClass()).add(elfo);
                    }
                }
            }
        }
    }
    
    private void renovarAtacantes(){
        this.dividirVivosPorClasse();
        this.atacantes.clear();
    }
    
    public void noturnosPorUltimo(){
        this.renovarAtacantes();
        this.atacantes.addAll(this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(0)));
        this.atacantes.addAll(this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(1)));
    }
    
    public void ataqueIntercalado(){
        this.renovarAtacantes();
        int qntElfosVerdes = this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(0)).size();
        int qntElfosNoturnos = this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(1)).size();
        int limitador = qntElfosVerdes < qntElfosNoturnos ? qntElfosVerdes : qntElfosNoturnos;
        for (int i = 0; i < limitador; i++){
            this.atacantes.add(this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(0)).get(i));
            this.atacantes.add(this.vivosPorClasse.get(TIPOS_PERMITIDOS.get(1)).get(i));
        }
    }
    
    public void ataqueSupremo(){
        this.renovarAtacantes();
        int numeroElfosNoturnos = (this.vivosPorClasse.get(ElfoNoturno.class).size());
        int numeroElfosVerdes = (this.vivosPorClasse.get(ElfoVerde.class).size());
        int totalElfos = numeroElfosNoturnos + numeroElfosVerdes;
        int limitadorElfosNoturnos = (int)((0.3 * numeroElfosVerdes) / 0.7);
        for (int i = 0; i < numeroElfosVerdes; i++){
            this.atacantes.add(this.vivosPorClasse.get(ElfoVerde.class).get(i));
        }
        for ( int i = 0; i < limitadorElfosNoturnos; i++ ){
            this.atacantes.add( this.vivosPorClasse.get( ElfoNoturno.class ).get(i) );
        }
        Collections.sort(atacantes, new OrdenarPorQtdFlecha());
    }
        
        
}


    
    
