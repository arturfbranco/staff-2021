import java.util.*;

public class ElfoVerde extends Elfo {
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Aço Valiriano",
            "Arco de Vidro",
            "Flecha de Vidro")
        );
    
    public ElfoVerde(String nome){
        super(nome);
        super.qntExperienciaPorAtaque = 2;
    }
    
    private boolean isDescricaoValida(String descricao){
        return DESCRICOES_VALIDAS.contains( descricao );
    }
    
    public void ganharItem(Item item){
        if (this.isDescricaoValida(item.getDescricao())) {
            super.inventario.adicionar( item );
        }
    }
    
    public void perderItem(Item item){
        if (this.isDescricaoValida(item.getDescricao())) {
            super.inventario.remover( item );
        }
    }
    
}