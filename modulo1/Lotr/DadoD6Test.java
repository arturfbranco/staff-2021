import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test{
    @Test
    public void dadoLancado10000Vezes(){
        boolean flag = true;
        for ( int i = 0; i < 200000; i++ ){
            int resultado = DadoD6.lancar();
            if (resultado != 1 && resultado != 2 && resultado != 3 && resultado !=4 && resultado != 5 && resultado != 6){
                flag = false;
            }
        }
        assertEquals(true, flag);
    }
}
