import java.util.*;

public class EstatisticasInventario{
    
    private Inventario inventario;
    
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    
    private boolean eVazio(){
        if (this.inventario.getItens().isEmpty()){
            return true;
        }
        return false;
    }
    
    public double calcularMedia(){
        if (this.eVazio()){
            return Double.NaN;
        }
        int somaQnts = 0;
        for (Item item : this.inventario.getItens()){
            somaQnts += item.getQuantidade();
        }
        return somaQnts / this.inventario.getItens().size();
    }
    
    public int qntItensAcimaDaMedia(){
        int acimaDaMedia = 0;
        double media = this.calcularMedia();
        for (Item item : this.inventario.getItens()){
            double quantidadeAtual = item.getQuantidade();
            if (quantidadeAtual > media){
                acimaDaMedia++;
            }
        }
        return acimaDaMedia;
    }
    
    public double calcularMediana(){
        if (this.eVazio()){
            return Double.NaN;
        }
        ArrayList<Integer> quantidadesOrdenadas = new ArrayList<>();
        for (Item item : this.inventario.getItens()){
            quantidadesOrdenadas.add(item.getQuantidade());
        }
        Collections.sort(quantidadesOrdenadas);
        int qntItens = quantidadesOrdenadas.size();
        int meio = qntItens / 2;
        int qntMeio = quantidadesOrdenadas.get(meio);
        boolean qntImpar = qntItens % 2 == 1;
        if (qntImpar){
            return qntMeio;
        }
        
        int qntMeioMenosUm = quantidadesOrdenadas.get(meio - 1);
        return(qntMeio + qntMeioMenosUm) / 2.0;
    }
    
    
}
