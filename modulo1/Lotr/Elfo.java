public class Elfo extends Personagem {
    private static int qntElfos;

    public Elfo( String nome ){
       super(nome);
       super.vida = 100.0;
       super.inventario.adicionar(new Item(1, "Arco"));
       super.inventario.adicionar(new Item(2, "Flecha"));
       Elfo.qntElfos++;
    }  
    
    public static int getQuantidade(){
        return Elfo.qntElfos;
    }
    
    public void finalize() throws Throwable{
        Elfo.qntElfos--;
    }
    
    public Item getFlecha(){
        return super.inventario.buscar("Flecha");
    }
    
    public int getQntFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    protected boolean podeAtirar(){
        return this.getQntFlecha() > 0;
    }
    
    public void atirarFlecha( Anao anao ){
        if (this.podeAtirar()){
            this.getFlecha().setQuantidade(this.getQntFlecha() - 1);
            super.aumentarXP();
            anao.sofrerDano();
            super.sofrerDano();
        }   
    }
    
    public void setQntDano(double qntDano){
        super.qntDano = qntDano;
    }
   
}
