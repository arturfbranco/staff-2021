

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class PaginadorInventarioTest{
    
   @Test
   public void pularItensDoInventarioPaginado(){
        Inventario inventario = new Inventario();
        Item garrafa = new Item(2, "Garrafa");
        Item espada = new Item (1, "Espada");
        Item cigarro = new Item (20, "Cigarro");
        Item isqueiro = new Item(1, "Isqueiro");
        Item lanca = new Item (2, "Lança");
        Item lanterna = new Item(3, "Lanterna");
        inventario.adicionar(garrafa);
        inventario.adicionar(espada);
        inventario.adicionar(cigarro);
        inventario.adicionar(isqueiro);
        inventario.adicionar(lanca);
        inventario.adicionar(lanterna);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(2);
        assertEquals(cigarro, paginador.getPaginadorInventario().getItens().get(paginador.getMarcador())); 
    }
    
   @Test
   public void limitarItensDoInventarioPaginado(){
        Inventario inventario = new Inventario();
        Item garrafa = new Item(2, "Garrafa");
        Item espada = new Item (1, "Espada");
        Item cigarro = new Item (20, "Cigarro");
        Item isqueiro = new Item(1, "Isqueiro");
        Item lanca = new Item (2, "Lança");
        Item lanterna = new Item(3, "Lanterna");
        inventario.adicionar(garrafa);
        inventario.adicionar(espada);
        inventario.adicionar(cigarro);
        inventario.adicionar(isqueiro);
        inventario.adicionar(lanca);
        inventario.adicionar(lanterna);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(2);
        ArrayList<Item> listaLimitada = paginador.limitar(3);
        assertEquals(cigarro, listaLimitada.get(0));
        assertEquals(isqueiro, listaLimitada.get(1));
        assertEquals(lanca, listaLimitada.get(2));
        assertEquals(3, listaLimitada.size());
    }
    
    @Test
   public void limitarItensDoInventarioPaginadoComLimitadorMaiorQueTamanhoDoArray(){
        Inventario inventario = new Inventario();
        Item garrafa = new Item(2, "Garrafa");
        Item espada = new Item (1, "Espada");
        Item cigarro = new Item (20, "Cigarro");
        Item isqueiro = new Item(1, "Isqueiro");
        Item lanca = new Item (2, "Lança");
        Item lanterna = new Item(3, "Lanterna");
        inventario.adicionar(garrafa);
        inventario.adicionar(espada);
        inventario.adicionar(cigarro);
        inventario.adicionar(isqueiro);
        inventario.adicionar(lanca);
        inventario.adicionar(lanterna);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(3);
        ArrayList<Item> listaLimitada = paginador.limitar(10);
        assertEquals(isqueiro, listaLimitada.get(0));
        assertEquals(lanca, listaLimitada.get(1));
        assertEquals(3, listaLimitada.size());
    }
}