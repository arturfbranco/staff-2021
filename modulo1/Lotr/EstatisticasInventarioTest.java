

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest{
    
    @Test
    public void calculoDaMediaQuantidadesInventario(){
        Inventario inventario = new Inventario();
        Item espada = new Item (5, "Espada");
        Item escudo = new Item (7, "Escudo");
        Item lanca = new Item (12, "Lança");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double media = estatisticas.calcularMedia();
        assertEquals(8, media, 0.1);  
    }
    
    @Test
    public void quantidadeDeItensAcimaDaMedia(){
        Inventario inventario = new Inventario();
        Item espada = new Item (1, "Espada");
        Item escudo = new Item (2, "Escudo");
        Item lanca = new Item (5, "Lança");
        Item isqueiro = new Item (5, "Isqueiro");
        Item cigarro = new Item (12, "Cigarro");
        Item garrafa = new Item (2, "Garrafa"); //Media = 4.5
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        inventario.adicionar(isqueiro);
        inventario.adicionar(cigarro);
        inventario.adicionar(garrafa);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double acimaDaMedia = estatisticas.qntItensAcimaDaMedia();
        assertEquals(3.0, acimaDaMedia, 0.1);
        
    }
    
    @Test
    public void calculoDamedianaImparQuantidadesInventario(){
        Inventario inventario = new Inventario();
        Item espada = new Item (1, "Espada");
        Item escudo = new Item (2, "Escudo");
        Item lanca = new Item (5, "Lança");
        Item isqueiro = new Item (6, "Isqueiro");
        Item cigarro = new Item (12, "Cigarro");
        Item garrafa = new Item (4, "Garrafa");
        Item roupa = new Item (3, "Roupa");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);       //1  2  3  4  5  6  12  => Mediana = 4
        inventario.adicionar(lanca);
        inventario.adicionar(isqueiro);
        inventario.adicionar(cigarro);
        inventario.adicionar(garrafa);
        inventario.adicionar(roupa);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double mediana = estatisticas.calcularMediana();
        assertEquals(4, mediana, 0.1);
        
    }
    
     @Test
    public void calculoDamedianaParQuantidadesInventario(){
        Inventario inventario = new Inventario();
        Item espada = new Item (1, "Espada");
        Item escudo = new Item (2, "Escudo");
        Item lanca = new Item (5, "Lança");
        Item isqueiro = new Item (6, "Isqueiro");
        Item cigarro = new Item (12, "Cigarro");
        Item garrafa = new Item (4, "Garrafa");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);       //1, 2, 4, 5, 6, 12  => Mediana = 4.5
        inventario.adicionar(lanca);
        inventario.adicionar(isqueiro);
        inventario.adicionar(cigarro);
        inventario.adicionar(garrafa);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double mediana = estatisticas.calcularMediana();
        assertEquals(4.5, mediana, 0.1);
        
    }
    
    @Test
    public void calculoMedianaEmInventarioComApenasUmValor(){
        Inventario inventario = new Inventario();
        Item espada = new Item (3, "Espada");
        inventario.adicionar(espada);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(3, resultado, 0.1);
    }
    

}
