import java.util.*;

public class PaginadorInventario{
    
    private Inventario inventario;
    private int marcador;
    
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    
    public Inventario getPaginadorInventario(){
        return this.inventario;
    }
    
    public int getMarcador(){
        return this.marcador;
    }
    
    public void pular (int marcadorCorte){
        this.marcador = marcadorCorte > 0 ? marcadorCorte : 0;
    }   
    
    public ArrayList<Item> limitar(int limitador){
        ArrayList<Item> inventarioLimitado = new ArrayList<>();
        int limite = this.marcador + limitador;
        for (int i = this.marcador; i < limite && i < this.inventario.getItens().size(); i++){
            inventarioLimitado.add(this.inventario.obter(i));
        }
        return inventarioLimitado;
    } 
 
}
