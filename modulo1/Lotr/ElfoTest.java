import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoTest{
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void exemploMudancaValoresItem(){
        Elfo elfo = new Elfo("Legolas");
        Item flecha = elfo.getFlecha();
        flecha.setQuantidade(1000);
        assertEquals (1000, flecha.getQuantidade());
    }
    
    @Test
    
    public void elfoDeveNascerCom2Flechas(){
        Elfo elfo = new Elfo("Legolas");
        assertEquals(2, elfo.getFlecha().getQuantidade());
    }
    
    @Test
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaUmXP(){
        Elfo elfo = new Elfo("Legolas");
        Anao anao = new Anao ("Alvo");
        elfo.atirarFlecha(anao);
        assertEquals(1, elfo.getFlecha().getQuantidade());
        assertEquals(1, elfo.getExperiencia());
    }
    
    @Test
    public void elfoAtiraApenasONumeroCertoDeFlechas () {
        Elfo elfo = new Elfo ("Shooter");
        Anao anao = new Anao ("Alvo");
        int i = 3;
        while (i > 0){
            elfo.atirarFlecha(anao);
            i--;
        }
    
        assertEquals (0, elfo.getFlecha().getQuantidade());
        assertEquals (2, elfo.getExperiencia());
    }
    
    @Test
    public void elfoGanhaItemNovo(){
        Elfo elfo = new Elfo ("Legolas");
        elfo.ganharItem(new Item(5, "Pao"));
        assertEquals("Pao", elfo.getInventario().obter(2).getDescricao());
    }
    
    @Test
    public void elfoPerdeItem(){
        Elfo elfo = new Elfo ("Legolas");
        Item caneta = new Item (1, "Caneta");
        elfo.inventario.adicionar(caneta);
        elfo.perderItem(caneta);
        assertEquals(2, elfo.getInventario().getItens().size());
    }
    
    @Test
    public void contadorDaQuantidadeDeElfosNascidos(){
        Elfo elfo = new Elfo("Legolas");
        ElfoNoturno elfo2 = new ElfoNoturno("Pedro");
        ElfoDaLuz elfo3 = new ElfoDaLuz("Bernardo");
        ElfoVerde elfo4 = new ElfoVerde("Justin");
        assertEquals(4, Elfo.getQuantidade());
    }
    
    
}
