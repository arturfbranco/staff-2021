
public class Anao extends Personagem {
    private boolean escudoEquipado;
    
    public Anao ( String nome ){
        super(nome);
        super.status = Status.RECEM_CRIADO;
        super.vida = 110.0;
        super.inventario.adicionar(new Item(1, "Escudo"));
        this.escudoEquipado = false;
        super.qntDano = 10.0;
    }
    
    public void equiparEDesequiparEscudo(){
        this.escudoEquipado = !this.escudoEquipado;
        super.qntDano = this.escudoEquipado ? 5.0 : 10.0;
    }
}
