
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


public class InventarioTest{
    
    
    @Test
    public void adicionarUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada de aço");
        inventario.adicionar(item);
        assertEquals(item, inventario.getItens().get(0));
    }
    
    @Test
    public void adicionarDoisItensInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada de aço");
        Item item2 = new Item(3, "Flecha de Luz");
        inventario.adicionar(item);
        inventario.adicionar(item2);
        assertEquals(item, inventario.getItens().get(0));
        assertEquals(item2, inventario.getItens().get(1));
    }
    
    @Test
    public void obterUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada de aço");
        inventario.adicionar(item);
        assertEquals(item, inventario.obter(0));
    }
    
    @Test
    public void removerUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada de aço");
        inventario.adicionar(item);
        inventario.remover(0);
        assertEquals(true, inventario.getItens().isEmpty());
    }
    
    @Test
    public void getDescricoesVariosItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item (1, "Espada");
        Item escudo = new Item (2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada, Escudo", resultado);
    }
    
    @Test
    public void getItemMaiorQuantidade(){
        Inventario inventario = new Inventario();
        Item espada = new Item (1, "Espada");
        Item escudo = new Item (2, "Escudo");
        Item lanca = new Item (5, "Lança");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        Item maiorQtd = inventario.getItemMaiorQuantidade();
        assertEquals(lanca, maiorQtd);
    }
    
    @Test
    public void getItemMaiorQuantidadeComItensDeMesmaQuantidade(){
        Inventario inventario = new Inventario();
        Item espada = new Item (1, "Espada");
        Item escudo = new Item (2, "Escudo");
        Item lanca = new Item (2, "Lança");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        Item maiorQtd = inventario.getItemMaiorQuantidade();
        assertEquals(escudo, maiorQtd);
    }
    
    @Test
    public void buscarItemNoInventario(){
        Inventario inventario = new Inventario();
        Item espada = new Item (1, "Espada");
        Item escudo = new Item (2, "Escudo");
        Item lanca = new Item (2, "Lança");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        Item obtido = inventario.buscar("Escudo");
        assertEquals(escudo, obtido);      
    }
    
    @Test
    public void inverterInventario(){
        Inventario inventario = new Inventario();
        Item espada = new Item (1, "Espada");
        Item escudo = new Item (2, "Escudo");
        Item lanca = new Item (2, "Lança");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        ArrayList<Item> invertido = inventario.inverter();
        assertEquals(espada, invertido.get(2));
        assertEquals(escudo, invertido.get(1));
        assertEquals(lanca, invertido.get(0)); 
    }
    
    @Test
    public void ordenacaoDeItensPorQuantidade(){
        Inventario inventario = new Inventario();
        Item espada = new Item (6, "Espada");
        Item escudo = new Item (5, "Escudo");
        Item lanca = new Item (20, "Lança");
        Item isqueiro = new Item (6, "Isqueiro");
        Item cigarro = new Item (2, "Cigarro");
        Item garrafa = new Item (1, "Garrafa"); // garrafa, cigarro, escudo, espada ou isqueiro, lança
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        inventario.adicionar(isqueiro);
        inventario.adicionar(cigarro);
        inventario.adicionar(garrafa);
        inventario.ordenarItens(TipoOrdenacao.ASC);
        assertEquals(garrafa, inventario.getItens().get(0));
        assertEquals(cigarro, inventario.getItens().get(1));
        assertEquals(isqueiro, inventario.getItens().get(4));
        assertEquals(lanca, inventario.getItens().get(5));
        assertEquals(escudo, inventario.getItens().get(2));
        assertEquals(espada, inventario.getItens().get(3));
    }
    
    @Test
    public void ordenacaoDeItensPorQuantidadeDecrescente(){
        Inventario inventario = new Inventario();
        Item espada = new Item (12, "Espada");
        Item escudo = new Item (13, "Escudo");
        Item lanca = new Item (20, "Lança");
        Item isqueiro = new Item (22, "Isqueiro");
        Item cigarro = new Item (24, "Cigarro");
        Item garrafa = new Item (30, "Garrafa"); // garrafa, cigarro, escudo, espada ou isqueiro, lança
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(lanca);
        inventario.adicionar(isqueiro);
        inventario.adicionar(cigarro);
        inventario.adicionar(garrafa);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        assertEquals(garrafa, inventario.getItens().get(0));
        assertEquals(cigarro, inventario.getItens().get(1));
        assertEquals(isqueiro, inventario.getItens().get(2));
        assertEquals(lanca, inventario.getItens().get(3));
        assertEquals(escudo, inventario.getItens().get(4));
        assertEquals(espada, inventario.getItens().get(5));
    }
}
