package com.company.Java;

import java.util.ArrayList;

public class Inventario {

    private ArrayList<Item> itens;

    public Inventario (){
        this.itens = new ArrayList<>();
    }

    public void adicionar (Item item) {
        this.itens.add(item);
    }

    private Boolean posicaoExistente(int posicao){
        return posicao < this.itens.size();
    }

    public Item buscarPorPosicao (int posicao){
        if (posicaoExistente(posicao)){
            return this.itens.get(posicao);
        }
        return null;
    }

    public Item buscarPorDescricao (String descricao){
        for (Item item : this.itens){
            if (item.getDescricao().equals(descricao)){
                return item;
            }
        }
        return null;
    }

    public void remover(Item item){
        this.itens.remove(item);
    }

    public void remover (int posicao){
        if(this.posicaoExistente(posicao)){
            this.itens.remove(posicao);
        }
    }

    private boolean itemJaExistente(Item item) {
        return this.buscarPorDescricao(item.getDescricao()) != null;
    }

    private int buscarPosicao (Item item){
        for (int i = 0; i < this.getItens().size(); i++){
            if (this.getItens().get(i).equals(item)){
                return i;
            }
        }
        return -1;
    }

    public Inventario unir (Inventario outroInventario){
        Inventario inventario = new Inventario();
        inventario.setItens(this.itens);
        for (Item item : outroInventario.getItens()){
            if(inventario.itemJaExistente(item)) {
                int posicao = inventario.buscarPosicao(item);
                inventario.getItens().get(posicao).aumentarQuantidade(inventario.getItens().get(posicao).getQuantidade());
            } else {
                inventario.adicionar(item);
            }
        }
        return inventario;
    }

    public Inventario diferenciar (Inventario outroInventario){
        Inventario inventario = new Inventario();
        inventario.setItens(this.itens);
        for (Item item : outroInventario.getItens()){
            if (inventario.itemJaExistente(item)){
                inventario.remover(item);
            }
        }
        return inventario;
    }

    public Inventario cruzar (Inventario outroInventario) {
        Inventario inventario = new Inventario();
        for (Item item : this.getItens()) {
            if (outroInventario.itemJaExistente(item)){
                int posicaoOutroInventario = outroInventario.buscarPosicao(item);
                item.aumentarQuantidade(outroInventario.getItens().get(posicaoOutroInventario).getQuantidade());
                inventario.adicionar(item);
            }
        }
        return inventario;
    }

    public Inventario intersec (Inventario outroInventario){
        Inventario resultado = new Inventario();
        for (Item item : outroInventario.getItens()){
            if(this.itemJaExistente(item)){
                resultado.adicionar(item);
            }
        }
        return resultado;
    }

    public ArrayList<Item> getItens() {
        return itens;
    }

    public void setItens(ArrayList<Item> itens) {
        this.itens = itens;
    }
}
