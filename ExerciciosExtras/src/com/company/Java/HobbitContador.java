package com.company.Java;

import java.util.ArrayList;

public class HobbitContador {

    private static final int[] NUMEROS_PRIMOS = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67,
                                                    71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 139, 149,
                                                    151, 157, 163, 167, 173};


    public int calcularDiferenca(ArrayList<ArrayList<Integer>> arrayDePares){
        ArrayList<Integer> produtos = calcularProdutos(arrayDePares);
        ArrayList<Integer> mmc = calcularMMC(arrayDePares);
        ArrayList<Integer> diferencas = operarDiferencas(produtos, mmc);
        return somarDiferencas(diferencas);

    }

    private ArrayList<Integer> calcularProdutos (ArrayList<ArrayList<Integer>> arrayDePares){
        ArrayList<Integer> produtos = new ArrayList<>();
        for (ArrayList<Integer> par : arrayDePares){
            produtos.add(par.get(0) * par.get(1));
        }
        return produtos;
    }

    private ArrayList<Integer> calcularMMC (ArrayList<ArrayList<Integer>> arrayDePares){
        ArrayList<Integer> mmcs = new ArrayList<>();
        for (ArrayList<Integer> par : arrayDePares){
            ArrayList<Integer> primosDivisores = operarDivisoes(par);
            int mmc = 1;
            for (Integer primo : primosDivisores){
                mmc *= primo;
            }
            mmcs.add(mmc);
        }
        return mmcs;
    }

    private ArrayList<Integer> operarDivisoes(ArrayList<Integer> par){
        ArrayList<Integer> primosDivisores = new ArrayList<>();
        int numero1 = par.get(0);
        int numero2 = par.get(1);
        int contador = 0;
        while (numero1 != 1 || numero2 != 1){
            if (numero1 % NUMEROS_PRIMOS[contador] == 0 || numero2 % NUMEROS_PRIMOS[contador] == 0){
                primosDivisores.add(NUMEROS_PRIMOS[contador]);
                if (numero1 % NUMEROS_PRIMOS[contador] == 0){
                    numero1 /= NUMEROS_PRIMOS[contador];
                }
                if(numero2 % NUMEROS_PRIMOS[contador] == 0){
                    numero2 /= NUMEROS_PRIMOS[contador];
                }
            } else {
                contador++;
            }
        }
        return primosDivisores;
    }

    private ArrayList<Integer> operarDiferencas(ArrayList<Integer> produtos, ArrayList<Integer> mmc){
        ArrayList<Integer> resultado = new ArrayList<>();
        for (int i = 0; i < produtos.size(); i++){
            Integer diferenca = produtos.get(i) - mmc.get(i);
            resultado.add(diferenca);
        }
        return resultado;
    }

    private int somarDiferencas(ArrayList<Integer> diferencas){
        Integer resultado = 0;
        for (Integer diff : diferencas){
            resultado += diff;
        }
        return resultado;
    }
}
