package com.company.Test;

import static org.junit.Assert.*;
import org.junit.Test;

import com.company.Java.Numeros;

public class NumerosTest {

    @Test
    public void calcularMediaComNumeroSeguinte(){
        double[] valores = {1.0, 3.0, 5.0, 1.0, -10.0};
        Numeros numeros = new Numeros(valores);
        double[] esperado = {2.0, 4.0, 3.0, -4.5};
        double[] resultado = numeros.calcularMediaSeguinte();
        assertEquals(resultado[0], esperado[0], 0.1);
        assertEquals(resultado[1], esperado[1], 0.1);
        assertEquals(resultado[2], esperado[2], 0.1);
        assertEquals(resultado[3], esperado[3], 0.1);

    }
}
