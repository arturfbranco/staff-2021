Informações sobre o Servidor:

A aplicação de Back-End utilizada, tal como seu arquivo .jar e o seu Dockerfile, encontra-se na pasta "TrabalhoBackEnd" do presente repositório.

Abra o Terminal na pasta Raíz do projeto 'coworking' e digite os seguintes comandos:

docker build . -t ce-be

docker run -p 8080:8080 ce-be
