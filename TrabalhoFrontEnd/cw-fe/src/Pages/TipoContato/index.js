import React from "react";

import 'antd/dist/antd.css';

import ApiBuscar from "../../API/ApiBuscar";
import ApiSalvar from "../../API/ApiSalvar";
import TipoContatoModel from "../../Models/TipoContatoModel";
import Cabecalho from "../../Components/Cabecalho";
import { Row, Col } from "antd";

import '../Pages.css'

import Navigator from "../../Components/Navigator";
import InputSalvarEditar from "../../Midware/InputSalvarEditar";
import Tabela from "../../Midware/Tabela";
import MeuBotao from "../../Midware/MeuBotao";

export default class TipoContato extends React.Component {
  constructor(props) {
    super(props);
    this.salvarTipoContato = this.salvarTipoContato.bind(this);
    this.apiSalvar = new ApiSalvar();
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todos: '',
      idAEditar: '',
      tipoContatoEditado: ''
    }
  }

  componentDidMount() {
    this.buscarTodos();
  }


  salvarTipoContato(nome) {
    if (nome !== '') {
      const novoTipoContato = new TipoContatoModel(nome.nome.toLowerCase());
      this.apiSalvar.salvarTipoContato(novoTipoContato).then(() =>
        this.buscarTodos());
    }
  }

  editarTipoContato(editado) {
    if (editado.id !== null && editado.nome !== null) {
      const tipoContatoEditado = new TipoContatoModel(editado.nome);
      this.apiSalvar.editarTipoContato(tipoContatoEditado, editado.id).then(() =>
        this.buscarTodos());
    }
  }

  buscarTodos() {
    this.apiBuscar.buscarTiposContato().then(tiposContato => {
      this.setState(state => { return { ...state, todos: tiposContato } });
    })
  }

  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Tipos Contato' />
          <Row>
            <Col span={2} className='nav' >
              <Navigator />
            </Col>
            <Col span={8} className='tirar-do-header text-align inline-block' >
              <InputSalvarEditar
                titulo='Manipulador de tipos de contato'
                salvar={this.salvarTipoContato.bind(this)}
                labelSalvar='Digite o novo valor'
                editar={this.editarTipoContato.bind(this)}
                labelId='Digite o ID do item'
                labelValor='Digite o novo valor'
              />
              <MeuBotao endereco='/cadastro-cliente' nome='Voltar'/>
            </Col>
            <Col span={8} className='tirar-do-header margin-left-20' >
                {!this.state.todos ?
                  <h3>Carregando...</h3> : (
                    <Tabela dataSource={this.state.todos} colunas={[
                      {
                        title: 'ID',
                        dataIndex: 'id',
                        key: 'id'
                      },
                      {
                        title: 'Tipo Contato',
                        dataIndex: 'nome',
                        key: 'nome'
                      }
                    ]} />
                  )
                }
              </Col>
          </Row>

        </header>
      </div>
    )
  }
}

/**
 * <React.Fragment>
        <p>Salvar</p>
        <input type='text' placeholder='Novo Tipo Contato' onBlur={ evt => { this.salvarTipoContato( evt.target.value ) } } />
        <p>Editar</p>
        <input type='number' placeholder='Id' onBlur={ evt => this.setState( state => { return{ ...state, idAEditar: evt.target.value } } ) } />
        <input type='text' placeholder='Editar' onBlur={ evt => this.setState( state => { return{ ...state, tipoContatoEditado: evt.target.value } } ) } />
        <button onClick={ () => this.editarTipoContato() } >Editar</button>
        { this.state.tipoContatoSalvo ? (
          <div>
            <p>Id: { this.state.tipoContatoSalvo.id }</p>
            <p>Nome: { this.state.tipoContatoSalvo.nome }</p>
          </div>
        ) : '' }
        { !this.state.todos ?
          <h3>Aguarde...</h3> :

           this.state.todos.map( tp => {
             return(
              <p>Id: { tp.id } / Nome: { tp.nome }</p>
             )
           })

        }
      </React.Fragment>
 *
 *
 */