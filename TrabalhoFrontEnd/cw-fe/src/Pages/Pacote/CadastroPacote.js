import React from 'react';
import Cabecalho from '../../Components/Cabecalho';

import { Row, Col } from 'antd';

import PacoteModel from '../../Models/PacoteModel'

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import FormPacote from '../../Midware/FormPacote';
import FormEspacoPacote from '../../Midware/FormEspacoPacote';
import ApiSalvar from '../../API/ApiSalvar';
import MeuBotao from '../../Midware/MeuBotao';
import BotaoAzul from '../../Midware/BotaoAzul';

export default class CadastroPacote extends React.Component {
  constructor(props) {
    super(props);
    this.apiSalvar = new ApiSalvar();
    this.state = {
      valor: '',
      espacoPacotes: [],
      pacoteSalvo: '',
      mensagem: ''
    }
  }

  salvarValor(valor) {
    if (valor.valor !== '') {
      this.setState(state => {
        return {
          ...state,
          valor: valor,
          pacoteSalvo: ''
        }
      })
    }

  }

  salvarEspacoPacote(espacoPacote) {
    if (!Object.values(espacoPacote).includes(undefined)) {
      let epAtualizado = this.state.espacoPacotes;
      epAtualizado.push(espacoPacote);
      this.setState(state => {
        return {
          ...state,
          espacoPacotes: epAtualizado,
          pacoteSalvo: ''
        }
      })
    }
  }

  registrarPacote() {
    const valor = this.state.valor;
    const eps = this.state.espacoPacotes;
    const epsFormatado = this.formatarEps(eps);
    const novoPacote = new PacoteModel(valor.valor, epsFormatado);
    if (valor.id === undefined || valor.id === '') {
      this.salvarPacote(novoPacote)
    } else {
      this.editarPacote(novoPacote, valor.id);
    }
  }

  salvarPacote(novoPacote) {
    this.apiSalvar.salvarPacote(novoPacote).then(pacote => {
      const pacoteNovo = new PacoteModel(pacote.valor, pacote.espacoPacote, pacote.id);
      this.setState(state => {
        return {
          ...state,
          pacoteSalvo: pacoteNovo,
          valor: '',
          espacoPacotes: []
        }
      })
    }).catch(() => {
      this.dadosInvalidos();
    })
  }

  editarPacote( pacote, id ){
    this.apiSalvar.editarPacote( pacote, id ).then( pacote => {
      this.setState( state => {
        return{
          ...state,
          pacoteSalvo: pacote,
          valor: '',
          espacoPacotes: []
        }
      } )
    } ).catch( () => {
      this.dadosInvalidos()
    } )
  }

  formatarEps(eps) {
    return eps.map(ep => {
      let espaco = new Object();
      espaco.id = ep.espaco;
      let epFormatada = new Object();
      epFormatada.espaco = espaco;
      epFormatada.tipoContratacao = ep.tipoContratacao.toUpperCase();
      epFormatada.quantidade = ep.quantidade;
      epFormatada.prazo = ep.prazo;
      return epFormatada;
    })
  }

  dadosInvalidos() {
    this.setState(state => {
      return {
        ...state,
        mensagem: 'Dados rejeitados pelo servidor. \n Verifique-os e tente novamente.',
      }
    });
    setTimeout(() => {
      this.setState(state => {
        return {
          ...state,
          mensagem: ''
        }
      })
    }, 5000)
  }

  deletarEps() {
    this.setState(state => {
      return {
        ...state,
        espacoPacotes: ''
      }
    })
  }

  apagarDados() {
    this.setState(state => {
      return {
        ...state,
        pacoteSalvo: ''
      }
    })
  }

  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Cadastro Pacote' />
          <Row>
            <Col span={3} className='nav' >
              <Navigator />
            </Col >
            <Col span={10} className='tirar-do-header text-align inline-block' >
              <FormPacote salvar={this.salvarValor.bind(this)} />
              <FormEspacoPacote salvar={this.salvarEspacoPacote.bind(this)} acaoSecundaria={this.deletarEps.bind(this)} />
            </Col>
            <Col className='tirar-do-header text-align inline-block' >
              {this.state.valor || this.state.espacoPacotes.length > 0 ? (
                <div className='background-salmon margin-left-20' >
                  <h3>Confira os dados do Pacote antes de enviar</h3>
                  <p>{`Valor: ${this.state.valor.valor}`}</p>
                  {!this.state.espacoPacotes ?
                    ('') :
                    this.state.espacoPacotes.map(ep => {
                      return (
                        <div>
                          <p>----------------------------------------------------------------</p>
                          <p>ID Espaço: {ep.espaco} / Tipo de contratação: {ep.tipoContratacao}</p>
                          <p>Quantidade: {ep.quantidade} / Prazo: {ep.prazo}</p>
                          <p>{'\n'}</p>
                        </div>
                      )
                    })}
                  <MeuBotao nome='Os dados estão corretos' metodo={this.registrarPacote.bind(this)} />
                  <h4 className='vermelho' >{this.state.mensagem}</h4>
                </div>


              ) : ''}
              {this.state.pacoteSalvo ? (
                <div className='background-salmon margin-left-20' >
                  <h3>Pacote salvo com sucesso!</h3>
                  <p>{`ID gerado: ${this.state.pacoteSalvo.id}`}</p>
                  <p>{`Valor: ${this.state.pacoteSalvo.valor}`}</p>
                  <h4>Contratações de espaço:</h4>
                  {this.state.pacoteSalvo.espacoPacote.map(ep => {
                    return (
                      <div>
                        <p>-----------------------------------------------</p>
                        <p>ID Espaço: {ep.espaco.id}</p>
                        <p>Contratação: {ep.tipoContratacao}</p>
                        <p>Quantidade: {ep.quantidade}</p>
                        <p>Prazo: {ep.prazo}</p>
                      </div>

                    )
                  })
                  }
                  <BotaoAzul nome='Registrar um novo Pacote' metodo={this.apagarDados.bind(this)} />
                </div>
              ) : ''}
            </Col>
          </Row>
        </header>
      </div >
    );
  }
}