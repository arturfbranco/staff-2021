import React from 'react';
import Cabecalho from '../../Components/Cabecalho';
import ApiBuscar from '../../API/ApiBuscar';

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import Tabela from '../../Midware/Tabela';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.apiBuscar = new ApiBuscar();
    this.state = {
      dadosGerais: '',
      pagamentos: ''
    }
  }

  componentDidMount(){
    this.gerarDadosGerais();
    this.gerarPagamentos();
  }

  gerarDadosGerais(){
    this.apiBuscar.buscarClientes().then( clientes => {
      this.apiBuscar.buscarEspacos().then( espacos => {
        this.apiBuscar.buscarPacotes().then( pacotes => {
          this.apiBuscar.buscarContratacoes().then( contratacoes => {
            this.apiBuscar.buscarClientePacotes().then( clientePacotes => {
              this.apiBuscar.buscarSaldoCliente().then( saldoCliente => {

                this.processarDadosGerais( clientes, espacos, pacotes, contratacoes, clientePacotes, saldoCliente );
              } )
            } )
          } )
        } )
      } )
    } )
  }

  processarDadosGerais( clientes, espacos, pacotes, contratacoes, clientePacotes, saldoCliente ){
    let dadosGerais = [];
    const dados = {
      qntClientes: clientes.length,
      qntEspacos: espacos.length,
      qntPacotes: pacotes.length,
      qntContratacoes: contratacoes.length,
      qntClientePacotes: clientePacotes.length,
      qntSaldos: this.buscarQntSaldosAtivos( saldoCliente )
    }
    dadosGerais.push( dados );
    this.salvarDadosGerais( dadosGerais );
  }

  salvarDadosGerais( dados ){
    this.setState( state =>{
      return{
        ...state,
        dadosGerais: dados
      }
    } )
  }

  buscarQntSaldosAtivos( sc ){
    return sc.filter( sc => sc.quantidade > 0 && sc.tipoContratacao !== 'ESPECIAL' ).length;
  }

  gerarPagamentos(){
    this.apiBuscar.buscarPagamentos().then( pagamentos => {
      this.apiBuscar.buscarContratacoes().then( conts => {
        this.processarPagamentos( pagamentos, conts );
      } )
    } )
  }

  processarPagamentos( pagamentos, conts ){
    if( pagamentos < 1 ){
      const zeroPagamentos = [{
        qntPagamentos: 0,
        receitaTotal: 'R$ 0,00'
      }];
      this.salvarPagamentos( zeroPagamentos );
      return;
    }
    let dadosPagamentos = [];
    const receita = this.buscarReceitas( pagamentos, conts ).reduce( ( a, b ) => a + b );
    const pags = {
      qntPagamentos: pagamentos.length,
      receitaTotal: this.converterIntEmString( receita )
    }
    dadosPagamentos.push( pags );
    this.salvarPagamentos( dadosPagamentos );
  }

  salvarPagamentos( pags ){
    this.setState( state => {
      return{
        ...state,
        pagamentos: pags
      }
    } )
  }

  buscarReceitas( pags, conts ){
    return pags.map( pag => {
      return pag.clientePacote === null ? this.calcularContratacao( pag, conts ) : this.calcularClientePacote( pag );
    } )
  }

  calcularContratacao( pag, conts ){
    const valorCont = conts.filter( cont => cont.id === pag.contratacao.id ).map( cont => cont.valor )[0];
    return this.converterStringEmInt( valorCont );
  }

  calcularClientePacote( pag ){
    return this.converterStringEmInt( pag.clientePacote.pacote.valor ) * pag.clientePacote.quantidade;
  }

  
  converterStringEmInt( valor ){
    return parseInt( valor.split( ' ' )[1].replace( ',', '.' ) );
  }

  converterIntEmString( valor ){
    return `R$ ${ valor.toString().replace( '.', ',' ) }`;
  }

  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Home' />
          <Row>
            <Col span={3} className='nav' >
              <Navigator />
            </Col>
            <Col span={15} className='tirar-do-header text-align inline-block margin-left-20' >
              <Row >
                <Col className='display-block' span={24} >
                  <h2>Dados Gerais</h2>
                  <Tabela dataSource={this.state.dadosGerais} colunas={[
                    {
                      title: 'Quantidade Clientes',
                      dataIndex: 'qntClientes',
                      key: 'qntClientes'
                    },
                    {
                      title: 'Quantidade Espaços',
                      dataIndex: 'qntEspacos',
                      key: 'qntEspacos'
                    },
                    {
                      title: 'Quantidade Pacotes',
                      dataIndex: 'qntPacotes',
                      key: 'qntPacotes'
                    },
                    {
                      title: 'Quantidade Contratações Individuais',
                      dataIndex: 'qntContratacoes',
                      key: 'qntContratacoes'
                    },
                    {
                      title: 'Quantidade Contratações Pacotes',
                      dataIndex: 'qntClientePacotes',
                      key: 'qntClientePacotes'
                    },
                    {
                      title: 'Quantidade Saldos Ativos',
                      dataIndex: 'qntSaldos',
                      key: 'qntSaldos'
                    }
                  ]} />
                </Col>
              </Row>
              <Row>
              <Col className='margin-left-50 margin-20' span={ 10 } >
                  <h2>Financeiro Geral</h2>
                  <Tabela dataSource={this.state.pagamentos} colunas={[
                    {
                      title: 'Total de Pagamentos',
                      dataIndex: 'qntPagamentos',
                      key: 'qntPagamentos'
                    },
                    {
                      title: 'Receita Total',
                      dataIndex: 'receitaTotal',
                      key: 'receitaTotal'
                    }
                  ]} />
                </Col>
              </Row>
            </Col>

          </Row>
        </header>
      </div>
    );
  }
}
