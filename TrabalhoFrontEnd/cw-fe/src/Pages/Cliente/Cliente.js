import React from 'react';
import Cabecalho from '../../Components/Cabecalho';

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import ApiBuscar from '../../API/ApiBuscar';
import BotaoManejar from '../../Midware/BotaoManejar';
import TabelaClientes from '../../Midware/TabelaClientes';

export default class Cliente extends React.Component {
  constructor(props) {
    super(props);
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todos: ''
    }
  }

  componentDidMount() {
    this.buscarTodos();
  }

  buscarTodos(){
    this.apiBuscar.buscarClientes().then(clientes => {
      const clientesFormatado = this.adicionarKeys( clientes );
      this.setState(state => {
        return {
          ...state,
          todos: clientesFormatado
        }
      })
    })
  }

  adicionarKeys( clientes ){
    return clientes.map( cliente => {
      return{
        ...cliente,
        key: cliente.id
      }
    } )
  }



  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Clientes' />
          <Row>
            <Col span={3} className='nav' >
              <Navigator />
            </Col>
            <Col className='tirar-do-header margin-left-50' span={14} >
              <BotaoManejar endereco='/cadastro-cliente' nome='Manejar Clientes' />
              <TabelaClientes data={this.state.todos} />
            </Col>
          </Row>
        </header>
      </div>
    );
  }
}