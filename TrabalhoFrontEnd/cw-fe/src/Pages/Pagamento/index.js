import React from 'react';
import Cabecalho from '../../Components/Cabecalho';

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import ApiSalvar from '../../API/ApiSalvar';
import ApiBuscar from '../../API/ApiBuscar';
import Tabela from '../../Midware/Tabela';
import FormPagamento from '../../Midware/FormPagamento';
import PagamentoContratacao from '../../Models/PagamentoModels/PagamentoContratacao';
import PagamentoClientePacote from '../../Models/PagamentoModels/PagamentoClientePacote';

export default class Pagamento extends React.Component {
  constructor(props) {
    super(props);
    this.apiSalvar = new ApiSalvar();
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todos: '',
      detalhado: '',
      saldosGerados: '',
      mensagem: ''
    }
  }

  componentDidMount() {
    this.buscarTodos();
  }

  buscarTodos() {
    this.apiBuscar.buscarPagamentos().then(res => {
      const pagsFormatados = this.formatarPagamentos(res);
      this.setState(state => {
        return {
          ...state,
          todos: pagsFormatados
        }
      })
    })
  }

  formatarPagamentos(pagamentos) {
    return pagamentos.map(pag => {
      let isContratacao = pag.clientePacote === null;
      return {
        id: pag.id,
        clienteId: isContratacao ? pag.contratacao.cliente.id : pag.clientePacote.cliente.id,
        clienteNome: isContratacao ? pag.contratacao.cliente.nome : pag.clientePacote.cliente.nome,
        tipo: isContratacao ? 'Individual' : 'Pacote',
        servicoId: isContratacao ? pag.contratacao.id : pag.clientePacote.id,
        tipoPagamento: pag.tipoPagamento
      }
    })
  }

  registrarPagamento(pag) {
    const novoPagamento = this.construirPagamento(pag);
    this.apiSalvar.salvarPagamento(novoPagamento).then(res => {
      this.buscarTodos()
      let saldoFormatado = this.formatarSaldo(res.saldosGerado);
      this.setState(state => {
        return {
          ...state,
          saldosGerados: saldoFormatado
        }
      })
    }).catch(() => {
      this.setState( state => {
        return {
          ...state,
          mensagem: 'Dados rejeitados pelo servidor. Confira-os e tente novamente!'
        }
      })
      this.removerMensagem();
    })
  }

  construirPagamento(pag) {
    if (pag.clientePacote && pag.contratacao) {
      return;
    }
    const tipo = pag.tipoPagamento.toUpperCase();
    if (pag.contratacao !== undefined && pag.contratacao !== '' ) {
      let contratacaoId = new Object();
      contratacaoId.id = pag.contratacao;
      return new PagamentoContratacao(contratacaoId, tipo);
    }
    let clientePacoteId = new Object();
    clientePacoteId.id = pag.clientePacote;
    return new PagamentoClientePacote(clientePacoteId, tipo);
  }

  formatarSaldo(saldos) {
    return saldos.map(saldo => {
      return {
        clienteId: saldo.id_cliente,
        espacoNome: saldo.espaco.nome,
        tipoContratacao: saldo.tipoContratacao,
        quantidade: saldo.quantidade,
        vencimento: this.formatarData(saldo.vencimento)
      }
    })
  }

  formatarData(data) {
    return data.split('-').reverse().join('/');
  }



  removerMensagem() {
    setTimeout(() => {
      this.setState(state => {
        return {
          ...state,
          mensagem: ''
        }
      })
    }, 4000)
  }

  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Pagamentos' />
          <Row>
            <Col span={3} className='nav' >
              <Navigator />
            </Col>
            <Col span={18} className='tirar-do-header text-align inline-block' >
              <Row>
                <Col span={10} className='margin-left-20'>
                  <FormPagamento salvar={this.registrarPagamento.bind(this)} />
                  <span className='vermelho' >CUIDADO: Não é permitido adicionar crédito de tipos de contratações diferentes para o mesmo espaço simultaneamente! Confira o Saldo ativo do Cliente antes de pagar uma nova contratação.</span>
                  <p className='vermelho text-align' >{this.state.mensagem}</p>
                </Col>
                <Col span={12} className='margin-left-20' >
                  {this.state.saldosGerados ? (
                    <div>
                      <h3>Saldo atualizado do cliente para os espaços pagos</h3>
                      <Tabela dataSource={this.state.saldosGerados} colunas={[
                        {
                          title: 'ID Cliente',
                          dataIndex: 'clienteId',
                          key: 'clienteId'
                        },
                        {
                          title: 'Espaço',
                          dataIndex: 'espacoNome',
                          key: 'espacoNome'
                        },
                        {
                          title: 'Tipo de Contratação',
                          dataIndex: 'tipoContratacao',
                          key: 'tipoContratacao'
                        },
                        {
                          title: 'Crédito total',
                          dataIndex: 'quantidade',
                          key: 'quantidade'
                        },
                        {
                          title: 'Vencimento',
                          dataIndex: 'vencimento',
                          key: 'vencimento'
                        }
                      ]} />
                    </div>
                  ) : ''}
                </Col>
              </Row>
              <Row className='container margin-20' >
                <Col span={18} >
                  <h2>Pagamentos processados</h2>
                  <Tabela dataSource={this.state.todos} colunas={[
                    {
                      title: 'ID',
                      dataIndex: 'id',
                      key: 'id'
                    },
                    {
                      title: 'ID Cliente',
                      dataIndex: 'clienteId',
                      key: 'clienteId'
                    },
                    {
                      title: 'Nome Cliente',
                      dataIndex: 'clienteNome',
                      key: 'clienteNome'
                    },
                    {
                      title: 'Modalidade',
                      dataIndex: 'tipo',
                      key: 'tipo'
                    },
                    {
                      title: 'ID Serviço',
                      dataIndex: 'servicoId',
                      key: 'servicoId'
                    },
                    {
                      title: 'Tipo de Pagamento',
                      dataIndex: 'tipoPagamento',
                      key: 'tipoPagamento'
                    }
                  ]} />
                </Col>
              </Row>
            </Col>
          </Row>
        </header>
      </div>
    );
  }
}
