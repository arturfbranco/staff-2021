import React from 'react';
import Cabecalho from '../../Components/Cabecalho';

import ContratacaoModel from '../../Models/ContratacaoModel'

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import FormContratacao from '../../Midware/FormContratacao';
import Tabela from '../../Midware/Tabela';
import ApiSalvar from '../../API/ApiSalvar';
import ApiBuscar from '../../API/ApiBuscar';
import CampoBusca from '../../Midware/CampoBusca';

export default class Contratacao extends React.Component {
  constructor(props) {
    super(props);
    this.apiSalvar = new ApiSalvar();
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todas: '',
      especifica: ''
    }
  }

  componentDidMount() {
    this.buscarTodas();
  }

  buscarTodas() {
    this.apiBuscar.buscarContratacoes().then(contratacoes => {
      const contratacoesFormatadas = contratacoes.map(cont => {
        return {
          ...cont,
          espaco: cont.espaco.id,
          cliente: cont.cliente.id,
          desconto: `${cont.desconto * 100}%`,
          prazo: `${cont.prazo} dias`
        }
      })
      this.setState(state => {
        return {
          ...state,
          todas: contratacoesFormatadas
        }
      })
    })
  }

  buscarPorId(id) {
    this.apiBuscar.buscarContratacaoPorId(id.id).then(res => {
      const resFormatada = { ...res, desconto: `${res.desconto * 100}%` };
      this.setState(state => {
        return {
          ...state,
          especifica: resFormatada
        }
      })
    })
  }

  registrarContratacao(data) {
    let idEspaco = new Object()
    idEspaco.id = data.espaco;
    let idCliente = new Object();
    idCliente.id = data.cliente
    const novaContratacao = new ContratacaoModel(idEspaco, idCliente,
      data.tipoContratacao.toUpperCase(),
      data.quantidade, this.trocarVirgulaPorPonto( data.desconto ), data.prazo
    );
    if (data.id === undefined) {
      this.apiSalvar.salvarContratacao(novaContratacao).then(res => {
        this.buscarTodas();
      })
    } else {
      this.apiSalvar.editarContratacao(novaContratacao, data.id).then(res => {
        this.buscarTodas();
      })
    }

  }

  trocarVirgulaPorPonto( valor ){
    return valor.replace( ',', '.' )
  }

  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Contratações' />
          <Row>
            <Col span={3} className='nav' >
              <Navigator />
            </Col>
            <Col span={18} className='tirar-do-header text-align inline-block' >
              <Row className='container margin-20 display-block' >
                <h1>Contratações Registradas</h1>
                <Tabela dataSource={this.state.todas} colunas={[
                  {
                    title: 'ID',
                    dataIndex: 'id',
                    key: 'id'
                  },
                  {
                    title: 'ID Espaço',
                    dataIndex: 'espaco',
                    key: 'espaco'
                  },
                  {
                    title: 'ID Cliente',
                    dataIndex: 'cliente',
                    key: 'cliente'
                  },
                  {
                    title: 'Tipo de Contratação',
                    dataIndex: 'tipoContratacao',
                    key: 'tipoContratacao'
                  },
                  {
                    title: 'Quantidade',
                    dataIndex: 'quantidade',
                    key: 'quantidade'
                  },
                  {
                    title: 'Prazo',
                    dataIndex: 'prazo',
                    key: 'prazo'
                  },
                  {
                    title: 'Desconto',
                    dataIndex: 'desconto',
                    key: 'desconto'
                  },
                  {
                    title: 'Valor',
                    dataIndex: 'valor',
                    key: 'valor'
                  }
                ]} />
              </Row>
              <Row >
                <Col span={13} >
                  <FormContratacao salvar={this.registrarContratacao.bind(this)} />
                </Col>
                <Col span={10} >
                  <CampoBusca metodo={this.buscarPorId.bind(this)} titulo='Buscar Contratação' />
                  {this.state.especifica ? (

                    <div className='background-salmon' >
                      <h3>Dados da Contratação</h3>
                      <p>{`ID: ${this.state.especifica.id}`}</p>
                      <p>{`Contratação: ${this.state.especifica.tipoContratacao} / Quantidade: ${this.state.especifica.quantidade}`}</p>
                      <p>{`Desconto: ${this.state.especifica.desconto}`}</p>
                      <p>{`Prazo: ${this.state.especifica.prazo} dias`}</p>
                      <p>{`Valor: ${this.state.especifica.valor}`}</p>
                      <h4>Espaço:</h4>
                      <p>{`ID: ${this.state.especifica.espaco.id} / Nome: ${this.state.especifica.espaco.nome}`}</p>
                      <p>{`Quantidade de Pessoas: ${this.state.especifica.espaco.qntPessoas}`}</p>
                      <p>{`Valor ${this.state.especifica.espaco.valor}`}</p>
                      <h4>Cliente:</h4>
                      <p>{`ID: ${this.state.especifica.cliente.id} / Nome: ${this.state.especifica.cliente.nome}`}</p>
                      <p>{`CPF: ${this.state.especifica.cliente.cpf}`}</p>

                    </div>
                  ) : ''}
                </Col>
              </Row>

            </Col>
          </Row>
        </header>
      </div>
    );
  }
}