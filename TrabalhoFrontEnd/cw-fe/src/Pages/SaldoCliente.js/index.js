import React from 'react';
import Cabecalho from '../../Components/Cabecalho';
import Tabela from '../../Midware/Tabela'

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import ApiBuscar from '../../API/ApiBuscar';

export default class SaldoCliente extends React.Component {

  constructor( props ){
    super( props );
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todos: ''
    }
  }

  componentDidMount(){
    this.buscarTodos();
  }

  buscarTodos(){
    this.apiBuscar.buscarSaldoCliente().then( saldos => {
      const saldosFormatados = this.formatarSaldos( saldos );
      this.setState( state => {
        return{
          ...state,
          todos: saldosFormatados
        }
      } )
    } )
  }

  formatarSaldos( saldos ){
    return saldos.map( saldo => {
      return{
        clienteId: saldo.id_cliente,
        espacoId: saldo.id_espaco,
        clienteCpf: saldo.cliente.cpf,
        clienteNome: saldo.cliente.nome,
        espacoNome: saldo.espaco.nome,
        tipoContratacao: saldo.tipoContratacao,
        quantidade: saldo.quantidade,
        vencimento: this.formatarData( saldo.vencimento )
      }
    } )
  }

  formatarData( data ){
    return data.split( '-' ).reverse().join( '/' );
  }

  render(){
    return(
      <div className="general">
        <header>
        <Cabecalho nomePagina='Saldos' />
          <Row>
            <Col span={ 3 } className='nav' >
              <Navigator />
            </Col>
            <Col span={18} className='tirar-do-header text-align inline-block margin-left-20' >
              <h2>Saldos de Clientes por Espaço</h2>
              <Tabela dataSource={ this.state.todos } colunas={[
                    {
                      title: 'ID Cliente',
                      dataIndex: 'clienteId',
                      key: 'clienteId'
                    },
                    {
                      title: 'ID Espaço',
                      dataIndex: 'espacoId',
                      key: 'espacoId'
                    },
                    {
                      title: 'CPF Cliente',
                      dataIndex: 'clienteCpf',
                      key: 'clienteCpf'
                    },
                    {
                      title: 'Nome',
                      dataIndex: 'clienteNome',
                      key: 'clienteNome'
                    },
                    {
                      title: 'Espaço',
                      dataIndex: 'espacoNome',
                      key: 'espacoNome'
                    },
                    {
                      title: 'Tipo de Contratação',
                      dataIndex: 'tipoContratacao',
                      key: 'tipoContratacao'
                    },
                    {
                      title: 'Crédito',
                      dataIndex: 'quantidade',
                      key: 'quantidade'
                    },
                    {
                      title: 'Prazo de Vencimento',
                      dataIndex: 'vencimento',
                      key: 'vencimento'
                    }
                  ]}/>
            </Col>
          </Row>
        </header>
      </div>
    );
  }
}
