import axios from "axios";

export default class ApiBuscar{
  constructor(){
    this.url = 'http://localhost:8080/cw';
    this.token = localStorage.getItem( 'token' );
    this.config = { headers: { Authorization: this.token } };
  }

  buscarTiposContato(){
    return axios.get( `${ this.url }/tipoContato/`, this.config ).then( res => res.data );
  }

  buscarClientes(){
    return axios.get( `${ this.url }/cliente/`, this.config ).then( res => res.data );
  }

  buscarEspacos(){
    return axios.get( `${ this.url }/espaco/`, this.config ).then( res => res.data );
  }

  buscarPacotes(){
    return axios.get( `${ this.url }/pacote/`, this.config ).then( res => res.data );
  }

  buscarContratacoes(){
    return axios.get( `${ this.url }/contratacao/`, this.config ).then( res => res.data );
  }

  buscarContratacaoPorId( id ){
    return axios.get( `${ this.url }/contratacao/${ id }`, this.config ).then( res => res.data );
  }

  buscarClientePacotes(){
    return axios.get( `${ this.url }/clientePacote/`, this.config ).then( res => res.data );
  }

  buscarClientePacotePorId( id ){
    return axios.get( `${ this.url }/clientePacote/${ id }`, this.config ).then( res => res.data );
  }

  buscarPagamentos(){
    return axios.get( `${ this.url }/pagamento/`, this.config ).then( res => res.data );
  }

  buscarPagamentoPorId( id ){
    return axios.get( `${ this.url }/pagamento/${ id }`, this.config ).then( res => res.data );
  }

  buscarSaldoCliente(){
    return axios.get( `${ this.url }/saldo/`, this.config ).then( res => res.data );
  }

  buscarAcessos(){
    return axios.get( `${ this.url }/acesso/`, this.config ).then( res => res.data );
  }
  
}