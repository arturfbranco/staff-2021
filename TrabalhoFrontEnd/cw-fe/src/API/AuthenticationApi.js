import axios from 'axios';

export default class AuthenticationApi{
  constructor(){
    this.url = 'http://localhost:8080';
  }

  cadastrar( { nome, email, login, senha } ){
    return axios.post( `${ this.url }/cadastrar`, { nome, email, login, senha } ).then( res => res.data );
  }

  login( { login, senha } ){
    return axios.post( `${ this.url }/login`, { login, senha } );
  }
}