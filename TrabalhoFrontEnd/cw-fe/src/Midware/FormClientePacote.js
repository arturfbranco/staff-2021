import { Form, Input, Button } from 'antd';

const FormClientePacote = props => {

  const { salvar } = props;

  return (
    <div>
      <h1 type='flex' align='middle' justify='center'>Manipulador de Contratação de Pacotes</h1>
      <p>Para atualizar uma Contratação de Pacote já existente, preencha novamente todos os campos deste formulário
        e ao fim informe o seu ID.</p>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 12 }}
        onFinish={salvar}
      >

        <Form.Item
          label='ID Cliente'
          name="cliente"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='ID Pacote'
          name="pacote"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Quantidade'
          name="quantidade"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='ID'
          name="id"
        >
          <Input placeholder='APENAS PARA ATUALIZAR' />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Salvar Contratação de Pacote 
          </Button>
        </Form.Item>
      </Form>
    </div>

  )
}

export default FormClientePacote;