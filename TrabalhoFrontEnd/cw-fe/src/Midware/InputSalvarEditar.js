import { Form, Input, Button } from 'antd';

const InputSalvarEditar = props => {

  const { titulo, salvar, editar, labelSalvar, labelId, labelValor } = props;

  return (
    <div>
      <h1 type='flex' align='middle' justify='center'>{ titulo }</h1>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 12 }}
        wrapperCol={{ span: 6 }}
        onFinish={salvar}
      >

        <Form.Item
          label={labelSalvar}
          name="nome"
        >
          <Input />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 12,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Salvar
          </Button>
        </Form.Item>
      </Form>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 12 }}
        wrapperCol={{ span: 6 }}
        onFinish={ editar }

      >
        <Form.Item
          label={labelId}
          name="id"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={labelValor}
          name="nome"
        >
          <Input />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 12,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Editar
          </Button>
        </Form.Item>
      </Form>
    </div>

  )
}

export default InputSalvarEditar;