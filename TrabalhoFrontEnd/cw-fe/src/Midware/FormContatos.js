import { Form, Input, Button } from 'antd';

const FormContatos = props => {

  const { salvar, acaoSecundaria } = props;

  return (
    <div>
      <h1 type='flex' align='middle' justify='center'>Adicionar Contatos</h1>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 12 }}
        wrapperCol={{ span: 12 }}
        onFinish={salvar}
      >
        <Form.Item
          label='ID do Tipo de contato'
          name="tipoContato"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Contato'
          name="contato"
        >
          <Input />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 12,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Adicionar contato
          </Button>
          
        </Form.Item>
        
      </Form>
      
    </div>
  )
}

export default FormContatos;