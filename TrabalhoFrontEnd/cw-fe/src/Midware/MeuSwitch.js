import React from 'react';

import { Switch } from 'antd';


const MeuSwitch = props => {

  const { pegar, textoTrue, textoFalse, estadoPadrao } = props;

  const [input, setInput] = React.useState(estadoPadrao);


  return (
    <Switch
      className='margin-20'
      checked={input}
      checkedChildren={textoTrue}
      unCheckedChildren={textoFalse}
      onChange={() => {
        setInput(!input);
        pegar(!input);
      }}
    />
  )
}

export default MeuSwitch;

