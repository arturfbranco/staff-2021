import { Form, Input, Button } from 'antd';

const FormPacote = props => {

  const { salvar } = props;

  return (
    <div>
      <h1 type='flex' align='middle' justify='center'>Dados do Pacote</h1>
      <p>Para atualizar um Pacote já existente, preencha novamente todos os campos deste formulário
        e informe o ID do Pacote.</p>
      <h4>Adicionar valor</h4>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 10 }}
        onFinish={salvar}
      >

        <Form.Item
          label='Valor'
          name="valor"
        >
          <Input placeholder='R$' />
        </Form.Item>
        <Form.Item
          label='ID do Pacote'
          name="id"
        >
          <Input placeholder='APENAS PARA ATUALIZAR' />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 9,
            span: 7,
          }}
        >
          <Button type="primary" htmlType="submit">
            Adicionar
          </Button>
        </Form.Item>
      </Form>
    </div>

  )
}

export default FormPacote;