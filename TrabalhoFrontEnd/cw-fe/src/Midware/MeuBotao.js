import { Button, Row, Col } from 'antd';

const MeuBotao = props => {

  const { nome, metodo, endereco, laranja } = props;

  return (
    <Row>
      <Col span={24}>
        <Button className='botao' href={ endereco }  onClick={ metodo } type="primary" danger >{nome}</Button>
      </Col>
    </Row>

  )
}

export default MeuBotao;