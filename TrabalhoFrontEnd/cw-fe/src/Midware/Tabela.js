import { Table } from 'antd';

const Tabela = props => {

  const { dataSource, colunas } = props;

  return(
    <Table dataSource={ dataSource } columns={ colunas } />
  )
}

export default Tabela;