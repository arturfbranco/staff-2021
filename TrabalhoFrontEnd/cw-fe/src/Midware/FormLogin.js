import { Form, Input, Button, Checkbox, Row, Col } from 'antd';
import MeuBotao from './MeuBotao';
import React from 'react';
import { Link } from 'react-router-dom';

const FormLogin = props => {

  const { onFinish } = props;



  return (
    <Form
      name="basic"
      layout='horizontal'
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 8 }}
      onFinish={onFinish}
    >
      <h1 type='flex' align='middle' justify='center'>Informe suas credenciais</h1>
      <Row type='flex' align='middle' justify='center' >
        <Col span={24}>
          <Form.Item
            label="Login"
            name="login"
            rules={[
              {
                required: true,
                message: 'Insira seu Login!',
              },
            ]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={24} >
          <Form.Item
            label="Senha"
            name="senha"
            rules={[
              {
                required: true,
                message: 'Insira sua Senha!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={24}>
          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Fazer Login
            </Button>
          </Form.Item>
        </Col>
        <Col span={ 24 } >
        <Form.Item
            wrapperCol={{
              offset: 8,
              span: 8,
            }}
          >
              <MeuBotao endereco='/cadastro' nome='Fazer Cadastro' />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )

}

export default FormLogin;