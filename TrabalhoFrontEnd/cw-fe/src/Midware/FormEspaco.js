import { Form, Input, Button } from 'antd';

const FormEspaco = props => {

  const { salvar } = props;

  return (
    <div>
      <h1 type='flex' align='middle' justify='center'>Manipulador de Espaços</h1>
      <p>Para atualizar um espaço já existente, preencha novamente todos os campos deste formulário
        e ao fim informe o seu ID.</p>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 12 }}
        onFinish={salvar}
      >

        <Form.Item
          label='Nome'
          name="nome"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Quantidade de pessoas'
          name="qntPessoas"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Valor'
          name="valor"
        >
          <Input placeholder='R$ ( por Minuto )' />
        </Form.Item>
        <Form.Item
          label='ID do Espaço'
          name="id"
        >
          <Input placeholder='APENAS PARA ATUALIZAR' />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Cadastrar Espaço
          </Button>
        </Form.Item>
      </Form>
    </div>

  )
}

export default FormEspaco;