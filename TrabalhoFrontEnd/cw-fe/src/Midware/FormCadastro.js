import { Form, Input, Button, Checkbox, Row, Col } from 'antd';

import { Link } from 'react-router-dom';

import React from 'react';

const FormCadastro = props => {

  const { onFinish } = props;

  return (
    <Form
      name="basic"
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <h1 type='flex' align='middle' justify='center'>Cadastramento de usuário</h1>

      <Row type='flex' align='middle' justify='center' >
        <Col span={12}>
          <Form.Item
            label="Nome"
            name="nome"
            rules={[{ required: true, message: 'Informe o seu nome!' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={12}>
          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: 'Informe o seu nome!' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={12}>
          <Form.Item
            label="Login"
            name="login"
            rules={[{ required: true, message: 'Informe o seu nome!' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={12}>
          <Form.Item
            label="Senha"
            name="senha"
            rules={[{ required: true, message: 'Informe a sua senha!' }]}
          >
            <Input.Password />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={12} >
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Cadastrar Usuário
            </Button>
            <Link to='/'>
              <Button type="primary" danger >
                Voltar para Login
              </Button>
            </Link>

          </Form.Item>
        </Col>
      </Row>

    </Form>
  )

}

export default FormCadastro;


/*
<React.Fragment>
      <h1 type='flex' align='middle' justify='center'>Cadastro</h1>
      <Form
      name="basic"
      initialValues={{
        remember: true,
      }}
      onFinish={ onFinish }
    >
        <Row type='flex' align='middle' justify='center'>
          <Col span={12}>
            <Form.Item>
              <label>Nome Completo</label>
              <Input placeholder='Ex. Nome Completo' />
            </Form.Item>
          </Col>
        </Row>


        <Row type='flex' align='middle' justify='center'>
          <Col span={12}>
            <label>E-mail</label>
            <Form.Item
              name="email"
              rules={[
                {
                  type: 'email',
                  message: 'Este e-mail não é valido!',
                },
                {
                  required: true,
                  message: 'Please input your E-mail!',
                },
              ]}
            >
              <Input placeholder='Ex. seuemail@gmail.com' />
            </Form.Item>
          </Col>
        </Row>
        <Row type='flex' align='middle' justify='center'>
          <Col span={12}>
            <label>Login</label>
            <Form.Item>
              <Input placeholder='Ex. Login de usuario' />
            </Form.Item>
          </Col>
        </Row>
        <Row type='flex' align='middle' justify='center'>
          <Col span={12}>
            <label>Senha</label>
            <Form.Item>
              <Input type='password' placeholder='Ex. Senha de usuario' />
            </Form.Item>
          </Col>
        </Row>
        <Row type='flex' align='middle' justify='center'>
          <Col>
            <Button type="primary">Cadastrar</Button>
          </Col>
          <Col style={{ marginLeft: '50px' }}>
            <Button type="primary" danger>Cancelar</Button>
          </Col>
        </Row>
      </Form>
    </React.Fragment>

*/