import { Form, Input, Button } from 'antd';

const FormAcesso = props => {

  const { salvar } = props;

  return (
    <div>      
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 12 }}
        onFinish={salvar}
      >

        <Form.Item
          label='ID Cliente'
          name="id_cliente"
        >
          <Input/>
        </Form.Item>
        <Form.Item
          label='ID Espaço'
          name="id_espaco"
        >
          <Input/>
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Registrar Acesso 
          </Button>
        </Form.Item>
      </Form>
    </div>

  )
}

export default FormAcesso;