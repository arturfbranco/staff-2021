import { Form, Input, Button } from 'antd';

const FormCliente = props => {

  const { salvar, acaoSecundaria } = props;

  return (
    <div>
      <h1 type='flex' align='middle' justify='center'>Dados Cadastrais</h1>
      <p>Para atualizar um cliente já existente, preencha novamente todos os campos deste formulário
        e ao fim informe o seu ID.</p>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 12 }}
        onFinish={salvar}
      >

        <Form.Item
          label='Nome'
          name="nome"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='CPF'
          name="cpf"
        >
          <Input placeholder='XXX.XXX.XXX-XX' />
        </Form.Item>
        <Form.Item
          label='Data de Nascimento'
          name="dataNascimento"
        >
          <Input placeholder='DD/MM/AAAA' />
        </Form.Item>
        <Form.Item
          label='ID do Cliente'
          name="id"
        >
          <Input placeholder='APENAS PARA ATUALIZAR' />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Adicionar Dados Cadastrais
          </Button>
        </Form.Item>
      </Form>
      <Button type="primary" onClick={ acaoSecundaria }>
            Limpar contatos
          </Button>
    </div>

  )
}

export default FormCliente;