export default class PacoteModel{
  constructor( valor, espacoPacote, id ){
    this.id = id ? id : '';
    this.valor = valor;
    this.espacoPacote = espacoPacote;
  }
}