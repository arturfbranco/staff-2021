package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Exception.InventarioErroSalvar;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "api/inventario")
public class InventarioController {

    @Autowired
    private InventarioService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public InventarioDTO salvarInventario(@RequestBody InventarioDTO inventario) throws InventarioErroSalvar {
        try{
            return service.salvar(inventario.converter());
        } catch(Exception e){
            throw new InventarioErroSalvar();
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public InventarioDTO editarInventario(@RequestBody InventarioDTO inventario, @PathVariable Integer id ){
        return service.editar(inventario.converter(), id);
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<InventarioDTO> buscarTodosInventarios(){
        return service.buscarTodos();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public InventarioDTO buscarInventarioPorId(@PathVariable Integer id) throws InventarioNaoEncontrado {
        try{
            return service.buscarPorId(id);
        } catch(Exception e){
            throw new InventarioNaoEncontrado();
        }
    }

    @PostMapping(value = "/buscarAnao")
    @ResponseBody
    public InventarioDTO buscarInventarioPorAnao (@RequestBody AnaoDTO anao){
        return service.buscarPorPersonagem(anao.converter());
    }

    @PostMapping(value = "/buscarElfo")
    @ResponseBody
    public InventarioDTO buscarInventarioPorElfo(@RequestBody ElfoDTO elfo){
        return service.buscarPorPersonagem(elfo.converter());
    }

    @PostMapping(value = "/buscarTodosId")
    @ResponseBody
    public List<InventarioDTO> buscarTodosPorId(@RequestBody List<Integer> ids){
        return service.buscarTodosPorId(ids);
    }

    @PostMapping(value = "/buscarInventarioItem")
    @ResponseBody
    public InventarioDTO buscarInventarioPorInventarioItem(@RequestBody List<Inventario_X_ItemDTO> inventarioItens){
        List<Inventario_X_ItemEntity> listaEntity = new ArrayList<>();
        for (Inventario_X_ItemDTO inventarioItem : inventarioItens){
            listaEntity.add(inventarioItem.converter());
        }
        return service.buscarPorInventarioItem(listaEntity);
    }

}
