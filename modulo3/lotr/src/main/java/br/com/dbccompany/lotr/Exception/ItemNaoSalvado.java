package br.com.dbccompany.lotr.Exception;

public class ItemNaoSalvado extends ItemException{

    public ItemNaoSalvado(){
        super("Impossível salvar o ítem solicitado!");
    }
}
