package br.com.dbccompany.lotr.DTO;


import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;

import java.util.ArrayList;
import java.util.List;

public class ItemDTO {

    private Integer id;
    private String descricao;
    private List<Inventario_X_ItemEntity> inventarioItem;


    public ItemDTO( ItemEntity item ) {
        this.id = item.getId();
        this.descricao = item.getDescricao();
        this.inventarioItem = item.getInventarioItem();
    }

    public ItemDTO(){}

    public ItemEntity converter(){
        ItemEntity item = new ItemEntity();
        item.setId(this.id);
        item.setDescricao(this.descricao);
        item.setInventarioItem(this.inventarioItem);
        return item;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Inventario_X_ItemEntity> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_ItemEntity> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }
}
