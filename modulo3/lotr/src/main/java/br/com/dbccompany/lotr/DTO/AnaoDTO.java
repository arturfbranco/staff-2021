package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;

public class AnaoDTO {

    private Integer id;
    private String nome;
    private Double vida = 110.0;
    private Integer experiencia = 0;
    private Double qntDano = 0.0;
    private Integer qntExperienciaPorAtaque = 1;
    private StatusEnum status = StatusEnum.RECEM_CRIADO;
    private InventarioEntity inventario;

    public AnaoDTO (AnaoEntity anao){
        this.id = anao.getId();
        this.nome = anao.getNome();
        this.vida = anao.getVida();
        this.experiencia = anao.getExperiencia();
        this.qntDano = anao.getQntDano();
        this.qntExperienciaPorAtaque = anao.getQntExperienciaPorAtaque();
        this.status = anao.getStatus();
        this.inventario = anao.getInventario();
    }

    public AnaoDTO (){}

    public AnaoEntity converter(){
        AnaoEntity anao = new AnaoEntity();
        anao.setId(this.id);
        anao.setNome(this.nome);
        anao.setVida(this.vida);
        anao.setExperiencia(this.experiencia);
        anao.setQntDano(this.qntDano);
        anao.setQntExperienciaPorAtaque(this.qntExperienciaPorAtaque);
        anao.setStatus(this.status);
        anao.setInventario(this.inventario);
        return anao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getQntDano() {
        return qntDano;
    }

    public void setQntDano(Double qntDano) {
        this.qntDano = qntDano;
    }

    public Integer getQntExperienciaPorAtaque() {
        return qntExperienciaPorAtaque;
    }

    public void setQntExperienciaPorAtaque(Integer qntExperienciaPorAtaque) {
        this.qntExperienciaPorAtaque = qntExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
