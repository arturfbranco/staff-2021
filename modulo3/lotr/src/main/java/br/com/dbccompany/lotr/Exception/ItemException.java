package br.com.dbccompany.lotr.Exception;

public class ItemException extends Exception {

    private String mensagem;

    public ItemException( String mensagem ) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
