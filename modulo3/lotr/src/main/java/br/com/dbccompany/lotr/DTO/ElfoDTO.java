package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;

public class ElfoDTO {

    private Integer id;
    private String nome;
    private Double vida = 110.0;
    private Integer experiencia = 0;
    private Double qntDano = 0.0;
    private Integer qntExperienciaPorAtaque = 1;
    private StatusEnum status = StatusEnum.RECEM_CRIADO;
    private InventarioEntity inventario;

    public ElfoDTO (ElfoEntity elfo){
        this.id = elfo.getId();
        this.nome = elfo.getNome();
        this.vida = elfo.getVida();
        this.experiencia = elfo.getExperiencia();
        this.qntDano = elfo.getQntDano();
        this.qntExperienciaPorAtaque = elfo.getQntExperienciaPorAtaque();
        this.status = elfo.getStatus();
        this.inventario = elfo.getInventario();
    }

    public ElfoDTO(){}

    public ElfoEntity converter(){
        ElfoEntity elfo = new ElfoEntity();
        elfo.setId(this.id);
        elfo.setNome(this.nome);
        elfo.setVida(this.vida);
        elfo.setExperiencia(this.experiencia);
        elfo.setQntDano(this.qntDano);
        elfo.setQntExperienciaPorAtaque(this.qntExperienciaPorAtaque);
        elfo.setStatus(this.status);
        elfo.setInventario(this.inventario);
        return elfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getQntDano() {
        return qntDano;
    }

    public void setQntDano(Double qntDano) {
        this.qntDano = qntDano;
    }

    public Integer getQntExperienciaPorAtaque() {
        return qntExperienciaPorAtaque;
    }

    public void setQntExperienciaPorAtaque(Integer qntExperienciaPorAtaque) {
        this.qntExperienciaPorAtaque = qntExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
