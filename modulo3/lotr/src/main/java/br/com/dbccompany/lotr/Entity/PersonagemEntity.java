package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
@Inheritance( strategy =  InheritanceType.SINGLE_TABLE )
public abstract class PersonagemEntity {

    @Id
    @SequenceGenerator( name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE )
    protected Integer id;

    @Column(unique = true, nullable = false)
    protected String nome;

    @Column(nullable = false, precision = 4, scale = 2)
    protected Double vida;

    @Column(nullable = false)
    protected Integer experiencia = 0;

    @Column(nullable = false, precision = 4, scale = 2)
    protected Double qntDano = 0.0;

    @Column(nullable = false)
    protected Integer qntExperienciaPorAtaque = 1;

    @Enumerated( EnumType.STRING )
    protected StatusEnum status = StatusEnum.RECEM_CRIADO;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_inventario")
    protected InventarioEntity inventario;


    public PersonagemEntity(String nome){
        this.nome = nome;
    }

    public PersonagemEntity(){
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getQntDano() {
        return qntDano;
    }

    public void setQntDano(Double qntDano) {
        this.qntDano = qntDano;
    }

    public Integer getQntExperienciaPorAtaque() {
        return qntExperienciaPorAtaque;
    }

    public void setQntExperienciaPorAtaque(Integer qntExperienciaPorAtaque) {
        this.qntExperienciaPorAtaque = qntExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
