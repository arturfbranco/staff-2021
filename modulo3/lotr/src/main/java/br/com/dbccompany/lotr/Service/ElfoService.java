package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ElfoService extends PersonagemService<ElfoRepository, ElfoEntity>{

    public ElfoDTO salvarElfo (ElfoEntity elfo){
        return new ElfoDTO(super.salvar(elfo));
    }
    public ElfoDTO editarElfo (ElfoEntity elfo, Integer id){
        return new ElfoDTO(super.editar(elfo, id));
    }
    public ElfoDTO buscarElfoPorId (Integer id){
        return new ElfoDTO(super.buscarPorId(id));
    }

    private List<ElfoDTO> listaEntityParaDBO(List<ElfoEntity> listaEntity){
        List<ElfoDTO> elfosDTO = new ArrayList<>();
        for(ElfoEntity elfo : listaEntity){
            elfosDTO.add(new ElfoDTO(elfo));
        }
        return elfosDTO;
    }
    public List<ElfoDTO> buscarTodosElfosPorId(List<Integer> ids){
        return listaEntityParaDBO(super.buscarTodosPorId(ids));
    }

    public List<ElfoDTO> buscarTodosElfos(){
        return listaEntityParaDBO(super.buscarTodosPersonagens());
    }
    public ElfoDTO buscarElfoPorNome (String nome){
        return new ElfoDTO(super.buscarPorNome(nome));
    }
    public ElfoDTO buscarElfoPorExperiencia(Integer experiencia){
        return new ElfoDTO(super.buscarPorExperiencia(experiencia));
    }
    public List<ElfoDTO> buscarTodosElfosPorExperiencia(Integer experiencia){
        return listaEntityParaDBO(super.buscarTodosPorExperiencia(experiencia));
    }
    public List<ElfoDTO> buscarElfosPorExperienciaIn(List<Integer> experiencias) {
        return listaEntityParaDBO(super.buscarTodosPorExperienciaIn(experiencias));
    }
    public ElfoDTO buscarElfoPorVida(Double vida){
        return new ElfoDTO(super.buscarPorVida(vida));
    }
    public List<ElfoDTO> buscarTodosElfosPorVida(Double vida) {
        return listaEntityParaDBO(super.buscarTodosPorVida(vida));
    }
    public List<ElfoDTO> buscarElfosPorVidaIn(List<Double> vidas) {
        return listaEntityParaDBO(super.buscarTodosPorVidaIn(vidas));
    }
    public ElfoDTO buscarElfoPorQntDano(Double qntDano){
        return new ElfoDTO(super.buscarPorQntDano(qntDano));
    }
    public List<ElfoDTO> buscarTodosElfosPorQntDano(Double qntDano){
        return listaEntityParaDBO(super.buscarTodosPorQntDano(qntDano));
    }
    public List<ElfoDTO> buscarElfosPorQntDanoIn (List<Double> qntsDano) {
        return listaEntityParaDBO(super.buscarTodosPorQntDanoIn(qntsDano));
    }
    public ElfoDTO buscarElfoPorQntExperienciaPorAtaque (Integer qntExperienciaPorAtaque){
        return new ElfoDTO(super.buscarPorQntExperienciaPorAtaque(qntExperienciaPorAtaque));
    }
    public List<ElfoDTO> buscarTodosElfosPorQntExperienciaPorAtaque(Integer qntExperienciaPorAtaque){
        return listaEntityParaDBO(super.buscarTodosPorQntExperienciaPorAtaque(qntExperienciaPorAtaque));
    }
    public List<ElfoDTO> buscarElfosPorQntExperienciaPorAtaqueIn(List<Integer> qntsExperienciaPorAtaque){
        return listaEntityParaDBO(super.buscarTodosQntExperienciaPorAtaqueIn(qntsExperienciaPorAtaque));
    }
    public ElfoDTO buscarElfoPorStatus(StatusEnum status){
        return new ElfoDTO(super.buscarPorStatus(status));
    }
    public List<ElfoDTO> buscarTodosElfosPorStatus(StatusEnum status){
        return listaEntityParaDBO(super.buscarTodosPorStatus(status));
    }
    public List<ElfoDTO> buscarElfosPorStatusIn(List<StatusEnum> status){
        return listaEntityParaDBO(super.buscarTodosPorStatusIn(status));
    }

}
