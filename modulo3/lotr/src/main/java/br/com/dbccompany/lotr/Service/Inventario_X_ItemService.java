package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.InventarioItemErroSalvar;
import br.com.dbccompany.lotr.Exception.InventarioItemNaoEncontrado;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Inventario_X_ItemService{

    @Autowired
    private Inventario_X_ItemRepository repository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private InventarioRepository invRepository;

    @Transactional( rollbackFor = Exception.class )
    public Inventario_X_ItemDTO salvar(Inventario_X_ItemEntity inventarioItem ) {
        Inventario_X_ItemEntity novoInventarioItem = this.salvarEEditar(inventarioItem);
        return new Inventario_X_ItemDTO(novoInventarioItem);
    }
    @Transactional(rollbackFor = Exception.class)
    public Inventario_X_ItemDTO editar( Inventario_X_ItemEntity inventarioItem, Inventario_X_ItemId id){
        inventarioItem.setId(id);
        Inventario_X_ItemEntity novoInventarioItem = this.salvarEEditar(inventarioItem);
        return new Inventario_X_ItemDTO(novoInventarioItem);
    }
    @Transactional(rollbackFor = Exception.class)
    protected Inventario_X_ItemEntity salvarEEditar(Inventario_X_ItemEntity inventarioItem){
        Integer idItem = inventarioItem.getId().getId_item();
        ItemEntity item = itemRepository.findById(idItem).orElse(null);
        Integer idInventario = inventarioItem.getId().getId_inventario();
        InventarioEntity inventario = invRepository.findById(idInventario).orElse(null);
        inventarioItem.setItem(item);
        inventarioItem.setInventario(inventario);
        return repository.save (inventarioItem);
    }

    private List<Inventario_X_ItemDTO> listaEntityParaDBO(List<Inventario_X_ItemEntity> listaEntity){
        List<Inventario_X_ItemDTO> inventarioItensDTO = new ArrayList<>();
        for (Inventario_X_ItemEntity inventarioItem : listaEntity){
            inventarioItensDTO.add(new Inventario_X_ItemDTO(inventarioItem));
        }
        return inventarioItensDTO;
    }
    public List<Inventario_X_ItemDTO> buscarTodos(){
        return listaEntityParaDBO(repository.findAll());
    }

    public Inventario_X_ItemDTO buscarPorId(Inventario_X_ItemId id) {
        Optional<Inventario_X_ItemEntity> inventarioItem = repository.findById(id);
        if(inventarioItem.isPresent()){
            return new Inventario_X_ItemDTO(repository.findById(id).get());
        }
        return null;
    }
    public Inventario_X_ItemDTO buscarPorQuantidade(Integer quantidade){
        return new Inventario_X_ItemDTO(repository.findByQuantidade(quantidade));
    }
    public List<Inventario_X_ItemDTO> buscarTodosPorQuantidade(Integer quantidade){
        return listaEntityParaDBO(repository.findAllByQuantidade(quantidade));
    }
    public List<Inventario_X_ItemDTO> buscarTodosPorQuantidades(List<Integer> quantidades){
        return listaEntityParaDBO(repository.findByQuantidadeIn(quantidades));
    }

    public Inventario_X_ItemDTO buscarPorInventario(InventarioEntity inventario){
        return new Inventario_X_ItemDTO(repository.findByInventario(inventario));
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorInventario (InventarioEntity inventario){
        return listaEntityParaDBO(repository.findAllByInventario(inventario));
    }

    public List<Inventario_X_ItemDTO> buscarPorInventarioIn (List<InventarioEntity> inventarios){
        return listaEntityParaDBO(repository.findByInventarioIn(inventarios));
    }

    public Inventario_X_ItemDTO buscarPorItem (ItemEntity item){
        return new Inventario_X_ItemDTO(repository.findByItem(item));
    }

    public List<Inventario_X_ItemDTO> buscarTodosPorItem (ItemEntity item){
        return listaEntityParaDBO(repository.findAllByItem(item));
    }

    public List<Inventario_X_ItemDTO> buscarPorItemIn(List<ItemEntity> itens){
        return listaEntityParaDBO(repository.findByItemIn(itens));
    }
}
