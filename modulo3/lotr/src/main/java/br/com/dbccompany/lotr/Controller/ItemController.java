package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Exception.ItemNaoSalvado;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Service.ItemService;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping( value = "/api/item" )
public class ItemController {

    @Autowired
    private ItemService service;

    private Logger logger = LoggerFactory.getLogger(LotrApplication.class);


    @PostMapping(value = "/salvar")
    @ResponseBody
    public ItemDTO salvarItem (@RequestBody ItemDTO item) throws ItemNaoSalvado {
        try{
            return service.salvar( item.converter() );
        } catch (Exception e){
            throw new ItemNaoSalvado();
        }
    }


    @PutMapping( value = "/editar/{ id }" )
    @ResponseBody
    public ItemDTO editarItem( @RequestBody ItemDTO item, @PathVariable Integer id ){
        try{
            return service.editar(item.converter(), id);
        } catch(ItemNaoSalvado e){
            System.err.println(e.getMensagem());
            return null;
        }
    }

    @GetMapping( value = "/" )
    @ResponseBody
    public List<ItemDTO> buscarTodosItens (){
        return service.buscarTodosItens();
    }


    @GetMapping (value = "/buscarId/{id}")
    @ResponseBody
    public ResponseEntity<ItemDTO> buscarItemPorId(@PathVariable Integer id){
        logger.info("Começou a buscar item específico");
        try{
            logger.info("Deu certo?");
            return new ResponseEntity<>(service.buscarPorId(id), HttpStatus.ACCEPTED);
        } catch(ItemNaoEncontrado e){
            logger.info("Item não encontrado.");
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/buscarDescricao/{descricao}")
    @ResponseBody
    public ItemDTO buscarItemPorDescricao(@PathVariable String descricao){
        return service.buscarPorDescricao(descricao);
    }

    @GetMapping(value = "/buscarTodos/{descricao}")
    @ResponseBody
    public List<ItemDTO> buscarTodosItensPorDescricao(@PathVariable String descricao){
        return service.buscarTodosPorDescricao(descricao);
    }

    @PostMapping(value = "/buscarIds")
    @ResponseBody
    public List<ItemDTO> buscarItensPorId(@RequestBody List<Integer> ids){
        return service.buscarPorIds(ids);
    }

    @PostMapping(value = "/buscarInventarioItem")
    @ResponseBody
    public ItemDTO buscarItemPorInventarioItem(@RequestBody List<Inventario_X_ItemDTO> inventariosItens){
        List<Inventario_X_ItemEntity> listaEntity = new ArrayList<>();
        for (Inventario_X_ItemDTO inventarioItem : inventariosItens){
            listaEntity.add(inventarioItem.converter());
        }
        return service.buscarPorInventarioItem(listaEntity);
    }


}
