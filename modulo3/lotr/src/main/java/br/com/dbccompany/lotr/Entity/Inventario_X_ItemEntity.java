package br.com.dbccompany.lotr.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Inventario_X_ItemEntity {

    @EmbeddedId
    private Inventario_X_ItemId id;

    private Integer quantidade;

    @JsonIgnore
    @ManyToOne( fetch = FetchType.LAZY)
    @MapsId("id_inventario")
    private InventarioEntity inventario;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("id_item")
    private ItemEntity item;

    public Inventario_X_ItemId getId() {
        return id;
    }

    public void setId(Inventario_X_ItemId id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

    public ItemEntity getItem() {
        return item;
    }

    public void setItem(ItemEntity item) {
        this.item = item;
    }

}
