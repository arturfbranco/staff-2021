package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Exception.InventarioErroSalvar;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository repository;

    @Transactional( rollbackFor = Exception.class ) //Se der excessão, dá rollback no banco de dados. Do contrário, dá commit.
    public InventarioDTO salvar(InventarioEntity inventario ) {
        InventarioEntity novoInventario = this.salvarEEditar(inventario);
        return new InventarioDTO(inventario);
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioDTO editar( InventarioEntity inventario, Integer id){
        inventario.setId(id);
        InventarioEntity novoInventario = this.salvarEEditar(inventario);
        return new InventarioDTO(inventario);
    }

    @Transactional(rollbackFor = Exception.class)
    protected InventarioEntity salvarEEditar(InventarioEntity inventario){
        return repository.save (inventario);
    }

    private List<InventarioDTO> listaEntityParaDBO(List<InventarioEntity> listaEntity){
        List<InventarioDTO> inventariosDTO = new ArrayList<>();
        for (InventarioEntity inventario : listaEntity){
            inventariosDTO.add(new InventarioDTO(inventario));
        }
        return inventariosDTO;
    }

    public List<InventarioDTO> buscarTodos(){
        return listaEntityParaDBO(repository.findAll());
    }

    public InventarioDTO buscarPorId(Integer id) {
        Optional<InventarioEntity> inventario = repository.findById(id);
        if(inventario.isPresent()){
            return new InventarioDTO(repository.findById(id).get());
        }
        return null;
    }

    public List<InventarioDTO> buscarTodosPorId(List<Integer> id){
        return listaEntityParaDBO(repository.findAllById(id));
    }

    public InventarioDTO buscarPorPersonagem(PersonagemEntity personagem){
        return new InventarioDTO(repository.findByPersonagem(personagem));
    }

    public List<InventarioDTO> buscarPorPersonagemIn (List<PersonagemEntity> personagens){
        return listaEntityParaDBO(repository.findByPersonagemIn(personagens));
    }

    public InventarioDTO buscarPorInventarioItem(List<Inventario_X_ItemEntity> inventarioItem){
        return new InventarioDTO(repository.findByInventarioItemIn(inventarioItem));
    }

}
