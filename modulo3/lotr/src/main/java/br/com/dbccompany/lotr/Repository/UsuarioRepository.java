package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {

    UsuarioEntity findByUsername(String username);
}
